package tiw1.maintenance.models;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Collections;
// import java.util.Date;
import java.util.Objects;

// import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = "allBatteries", query = "SELECT b FROM Batterie b"),
    @NamedQuery(name = "batterieById", query = "SELECT b FROM Batterie b where b.id=:id")
})
public class Batterie {

    @Id
    @GeneratedValue
    private long id;

    private boolean charging = false;

    private double chargeLevel = 0.0;

    // @ElementCollection
    // private Collection<Date> chargeHistory = new ArrayList<>();

    public Batterie() {
    }

    public Batterie(long id) {
        this.id = id;
    }

    public Batterie(long id, boolean charging, double chargeLevel/* , Collection<Date> chargeHistory */) {
        this.id = id;
        this.charging = charging;
        this.chargeLevel = chargeLevel;
        // this.chargeHistory = chargeHistory;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isCharging() {
        return this.charging;
    }

    public boolean getCharging() {
        return this.charging;
    }

    public void setCharging(boolean charging) {
        this.charging = charging;
    }

    public double getChargeLevel() {
        return this.chargeLevel;
    }

    public void setChargeLevel(double chargeLevel) {
        this.chargeLevel = chargeLevel;
    }

    // public Collection<Date> getChargeHistory() {
    //     return Collections.unmodifiableCollection(this.chargeHistory);
    // }

    // public void setChargeHistory(Collection<Date> chargeHistory) {
    //     this.chargeHistory = chargeHistory;
    // }

    public Batterie id(long id) {
        this.id = id;
        return this;
    }

    public Batterie charging(boolean charging) {
        this.charging = charging;
        return this;
    }

    public Batterie chargeLevel(double chargeLevel) {
        this.chargeLevel = chargeLevel;
        return this;
    }

    // public Batterie chargeHistory(Collection<Date> chargeHistory) {
    //     this.chargeHistory = chargeHistory;
    //     return this;
    // }

    public Batterie chargerBatterie() {
        setCharging(true);
        return this;
    }

    public Batterie finChargeBatterie() {
        setCharging(false);
        setChargeLevel(100.0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Batterie)) {
            return false;
        }
        Batterie batterie = (Batterie) o;
        return id == batterie.id && charging == batterie.charging && chargeLevel == batterie.chargeLevel /* && Objects.equals(chargeHistory, batterie.chargeHistory) */;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, charging, chargeLevel/* , chargeHistory */);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", charging='" + isCharging() + "'" +
            ", chargeLevel='" + getChargeLevel() + "'" +
            // ", chargeHistory='" + getChargeHistory() + "'" +
            "}";
    }
}
