package tiw1.maintenance.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Intervention {
    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date date;

    private String description;

    public static final String BATTERY_CHARGED = "Batterie chargée";
    public static final String BATTERY_CHARGING = "Mise en charge";
    public static final String BATTERY_REMOVED = "Retrait batterie";
    public static final String BATTERY_MOUNTED = "Installation batterie";

    public Intervention() {
    }

    public Intervention(Date date, String description) {
        this(null, date, description);
    }

    public Intervention(Long id, Date date, String description) {
        this.id = id;
        this.date = date;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
