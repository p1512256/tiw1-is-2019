package tiw1.maintenance.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Entity
@NamedQueries({
    @NamedQuery(name = "allTrottinettes", query = "SELECT t FROM Trottinette t"),
    @NamedQuery(name = "trottinetteById", query = "SELECT t FROM Trottinette t where t.id=:id")
})
public class Trottinette {
    @Id
    @GeneratedValue
    private long id;

    private boolean disponible = true;

    @OneToMany
    private Collection<Intervention> interventions = new ArrayList<>();

    @OneToOne
    private Batterie batterie = null;

    public Trottinette() {
    }

    public Trottinette(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public Collection<Intervention> getInterventions() {
        return Collections.unmodifiableCollection(interventions);
    }

    public Batterie getBatterie() {
        return this.batterie;
    }

    public void setBatterie(Batterie batterie) {
        this.batterie = batterie;
    }

    public void installerBatterie(Batterie batterie) {
        setBatterie(batterie);
    }

    public void retirerBatterie() {
        setBatterie(null);
    }

    public boolean hasBatterie() {
        return (getBatterie() == null);
    }

    public void ajouterIntervention(Intervention intervention) {
        interventions.add(intervention);
    }

    public void supprimerIntervention(Intervention intervention) {
        interventions.remove(intervention);
    }
}
