package tiw1.maintenance.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tiw1.maintenance.metier.Maintenance;
import tiw1.maintenance.models.Batterie;

import java.util.List;

@RestController
@RequestMapping("/batterie")
public class BatterieController {

    @Autowired
    private Maintenance m;

    @PostMapping
    public ResponseEntity<Batterie> addBatterie() {
        return new ResponseEntity<Batterie>(m.creerBatterie(), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Batterie> getBatterie(@PathVariable long id) {
        final Batterie batterie = m.getBatterie(id);
        if (batterie == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Batterie>(batterie, HttpStatus.OK);
        }
    }

    @GetMapping
    public List<Batterie> getAllBatteries() {
        List<Batterie> batteries = m.getBatteries();
        return batteries;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Batterie> updateBatterie(@PathVariable long id, @RequestBody Batterie b) {
        b.setId(id);
        Batterie resp = m.updateBatterie(b);
        return new ResponseEntity<Batterie>(resp, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBatterie(@PathVariable long id) {
        m.supprimerBatterie(id);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }
}
