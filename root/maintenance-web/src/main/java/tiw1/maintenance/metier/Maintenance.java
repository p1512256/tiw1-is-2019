package tiw1.maintenance.metier;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import tiw1.maintenance.models.Batterie;
import tiw1.maintenance.models.Intervention;
import tiw1.maintenance.models.Trottinette;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import java.util.Date;
import java.util.List;

@Component
public class Maintenance {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public List<Trottinette> getTrottinettes() {
        List<Trottinette> trottinettes = em.createNamedQuery("allTrottinettes", Trottinette.class).getResultList();
        // ensure trottinettes are fully fetched, including associated Intervention entities
        for (Trottinette t : trottinettes) {
            t.getInterventions().size();
        }
        return trottinettes;
    }

    @Transactional
    public Trottinette getTrottinetteAndInterventions(long id) {
        try {
            Trottinette t = em
                    .createNamedQuery("trottinetteById", Trottinette.class)
                    .setParameter("id", id)
                    .getSingleResult();
            // ensure trottinettes is fully fetched, including associated Intervention entities
            t.getInterventions().size();
            return t;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public Trottinette creerTrottinette() {
        Trottinette t = new Trottinette();
        em.persist(t);
        return t;
    }

    @Transactional
    public void supprimerTrottinette(Trottinette t) {
        em.remove(t);
    }

    @Transactional
    public void supprimerTrottinette(long id) {
        Trottinette t = getTrottinetteAndInterventions(id);
        supprimerTrottinette(t);
    }

    @Transactional
    public Trottinette updateTrottinette(Trottinette t) {
        Trottinette t2 = em.merge(t);
        // ensure trottinettes is fully fetched, including associated Intervention entities
        t2.getInterventions().size();
        return t2;
    }

    @Transactional
    public Trottinette ajouterIntervention(long idTrottinette, Intervention intervention) {
        Trottinette t = em.find(Trottinette.class, idTrottinette);
        if (t != null) {
            em.persist(intervention);
            t.ajouterIntervention(intervention);
        }
        return t;
    }

    @Transactional
    public List<Batterie> getBatteries() {
        List<Batterie> batteries = em
            .createNamedQuery("allBatteries", Batterie.class)
            .getResultList();
            // for (Batterie b : batteries) {
            //     b.getChargeHistory().size();
            // }
            return batteries;
    }

    @Transactional
    public Batterie getBatterie(long id) {
        try {
            Batterie b = em
                .createNamedQuery("batterieById", Batterie.class)
                .setParameter("id", id)
                .getSingleResult();
            // b.getChargeHistory().size();
            return b;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public Batterie creerBatterie() {
        Batterie b = new Batterie();
        em.persist(b);
        return b;
    }

    @Transactional
    public void supprimerBatterie(Batterie b) {
        em.remove(b);
    }

    @Transactional
    public void supprimerBatterie(long id) {
        Batterie b = getBatterie(id);
        supprimerBatterie(b);
    }

    @Transactional
    public Batterie updateBatterie(Batterie b) {
        Batterie b2 = em.merge(b);
        // b2.getChargeHistory();
        return b2;
    }

    @Transactional
    public Trottinette installerBatterie(Trottinette t, Batterie b) {
        Intervention i = new Intervention();
        i.setDate(new Date());
        i.setDescription(Intervention.BATTERY_MOUNTED);

        t.installerBatterie(b);
        updateTrottinette(t);
        ajouterIntervention(t.getId(), i);
        return t;
    }

    @Transactional
    public Trottinette retirerBatterie(Trottinette t) {
        Intervention i = new Intervention();
        i.setDate(new Date());
        i.setDescription(Intervention.BATTERY_REMOVED);

        t.retirerBatterie();
        updateTrottinette(t);
        ajouterIntervention(t.getId(), i);
        return t;
    }

    @Transactional
    public Batterie chargerBatterie(Batterie b) {
        b.chargerBatterie();
        updateBatterie(b);
        return b;
    }

    @Transactional
    public Batterie finChargeBatterie(Batterie b) {
        b.finChargeBatterie();
        updateBatterie(b);
        return b;
    }
}
