package tiw1.maintenance.metier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import tiw1.maintenance.models.Batterie;
import tiw1.maintenance.models.Intervention;
import tiw1.maintenance.models.Trottinette;
import tiw1.maintenance.spring.AppConfig;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class MaintenanceTest {

    @Autowired
    private Maintenance m;

    @Test
    public void checkContextSetup() {
        assertNotNull(m);
    }

    @Test
    public void testCreerSupprimerTrottinette() {
        Trottinette t = m.creerTrottinette();
        long id = t.getId();
        Trottinette t2 = m.getTrottinetteAndInterventions(id);
        assertEquals(id, t2.getId());
        m.supprimerTrottinette(id);
        assertNull(m.getTrottinetteAndInterventions(id));
    }

    @Test
    public void testUpdateTrottinette() {
        Trottinette t = m.creerTrottinette();
        long id = t.getId();
        boolean t_disp = t.isDisponible();
        Trottinette t2 = new Trottinette(id);
        t2.setDisponible(!t.isDisponible());
        Trottinette t3 = m.updateTrottinette(t2);
        assertNotEquals(t_disp, t3.isDisponible());

        m.supprimerTrottinette(t3.getId());
    }

    @Test
    public void testAjouterIntervention() {
        Trottinette trottinette = m.creerTrottinette();
        long idTrottinette = trottinette.getId();
        int interventionsCountBefore = trottinette.getInterventions().size();
        Intervention i = new Intervention();
        m.ajouterIntervention(idTrottinette, i);
        Trottinette t2 = m.getTrottinetteAndInterventions(idTrottinette);
        int interventionsCountAfter = t2.getInterventions().size();
        assertEquals(interventionsCountAfter, interventionsCountBefore + 1);

        m.supprimerTrottinette(t2.getId());
    }

    @Test
    public void testCreerSupprimerBatterie() {
        Batterie b = m.creerBatterie();
        long id = b.getId();
        Batterie b2 = m.getBatterie(id);
        assertEquals(id, b2.getId());
        m.supprimerBatterie(id);
        assertNull(m.getBatterie(id));
    }

    @Test
    public void testUpdateBatterie() {
        Batterie b = m.creerBatterie();
        long id = b.getId();
        boolean b_charging = b.isCharging();
        Batterie b2 = new Batterie(id);
        b2.setCharging(!b.isCharging());
        Batterie b3 = m.updateBatterie(b2);
        assertNotEquals(b_charging, b3.isCharging());

        m.supprimerBatterie(b3.getId());
    }

    @Test
    public void testInstallerBatterie() {
        Trottinette t = m.creerTrottinette();
        Batterie b = m.creerBatterie();

        long idT = t.getId();
        long idB = b.getId();

        assertNull(t.getBatterie());
        m.installerBatterie(t, b);

        Trottinette t2 = m.getTrottinetteAndInterventions(idT);
        assertNotNull(t2.getBatterie());
        assertEquals(idB, t2.getBatterie().getId());

        m.supprimerTrottinette(t2.getId());
        m.supprimerBatterie(b.getId());
    }

    @Test
    public void testRetirerBatterie() {
        Trottinette t = m.creerTrottinette();
        Batterie b = m.creerBatterie();

        long idT = t.getId();
        long idB = b.getId();

        m.installerBatterie(t, b);

        Trottinette t2 = m.getTrottinetteAndInterventions(idT);
        assertNotNull(t2.getBatterie());
        assertEquals(idB, t2.getBatterie().getId());

        m.retirerBatterie(t);
        Trottinette t3 = m.getTrottinetteAndInterventions(idT);
        assertNull(t3.getBatterie());

        m.supprimerTrottinette(t3.getId());
        m.supprimerBatterie(b.getId());
    }

    @Test
    public void testChargerBatterie() {
        Batterie b = m.creerBatterie();
        long id = b.getId();
        boolean b_charging = b.isCharging();
        assertFalse(b.isCharging());
        m.chargerBatterie(b);

        Batterie b2 = m.getBatterie(id);
        assertTrue(b2.isCharging());
        assertEquals(!b_charging, b2.isCharging());

        m.supprimerBatterie(b2.getId());
    }

    @Test
    public void testFinChargeBatterie() {
        Batterie b = m.creerBatterie();
        long id = b.getId();

        m.chargerBatterie(b);
        m.finChargeBatterie(b);

        Batterie b3 = m.getBatterie(id);
        assertFalse(b3.isCharging());
        assertEquals(100.0, b3.getChargeLevel(), 0.0);

        m.supprimerBatterie(b3.getId());
    }
}
