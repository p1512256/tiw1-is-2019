package tiw1.emprunt.refs.context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Context implements IContext {

    private Map<String, Object> references = new HashMap<>();
    private Context parentContext = null;

    public Context() {}
    public Context(Context parentContext) {
        this.parentContext = parentContext;
    }

    public Context getParentContext() {
        return this.parentContext;
    }

    @Override
    public Object get(String key) {
        List<String> partialKeys = new ArrayList<String>(Arrays.asList(key.split("/")));

        String keyHead = partialKeys.get(0);
        String keyTail = String.join("/", partialKeys.subList(1, partialKeys.size()));

        Object entryAtLevel = references.get(keyHead);

        if (partialKeys.size() == 1)    return entryAtLevel;    // Entry found
        if (entryAtLevel == null)       return null;            // Entry does not exist
        return ((IContext) entryAtLevel).get(keyTail);
    }

    @Override
    public void set(String key, Object ref) throws Exception {
        List<String> partialKeys = new ArrayList<String>(Arrays.asList(key.split("/")));

        String keyHead = partialKeys.get(0);
        String keyTail = String.join("/", partialKeys.subList(1, partialKeys.size()));

        Object entryAtLevel = references.get(keyHead);

        if (partialKeys.size() == 1) {
            if (entryAtLevel != null) throw new Exception("Une entrée correspondant à la clé " + keyHead + " existe déjà dans l'annuaire");

            references.put(keyHead, ref);
        } else {
            if (entryAtLevel == null) {
                entryAtLevel = new Context();
                references.put(keyHead, entryAtLevel);
            } else if (!Context.class.isInstance(entryAtLevel)) {
                throw new Exception("Une entrée correspondant à la clé " + keyHead + " existe déjà dans l'annuaire");
            }
            IContext subContext = (IContext) entryAtLevel;
            subContext.set(keyTail, ref);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append(": " + print(0));
        return sb.toString();
    }

    public String print(int indents) {
        StringBuilder sb = new StringBuilder();

        String indent = "";
        for (int i = 0; i < indents; i++) {
            indent += "    ";
        }

        sb.append("{\n");
        for (Map.Entry<String, Object> entry : references.entrySet()) {
            String keyStr = entry.getKey();
            String valueStr;
            if (Context.class.isInstance(entry.getValue())) {
                valueStr = ((Context) entry.getValue()).print(indents + 1);
            } else {
                valueStr = entry.getValue().toString();
            }
            sb.append(indent + "    " + keyStr + ": " + valueStr + "\n");
        }
        sb.append(indent + "}");

        return sb.toString();
    }
}
