package tiw1.emprunt.refs.annuaire;

import tiw1.emprunt.refs.context.Context;
import tiw1.emprunt.utils.observer.Observable;

public class Annuaire extends Observable implements IAnnuaire {

    private Context rootContext = new Context();

    @Override
    public Object find(String key) {
        return rootContext.get(key);
    }

    @Override
    public Object bind(String key, Object entry) throws Exception {
        rootContext.set(key, entry);
        notifyObservers(entry, key);
        return entry;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" + rootContext.toString();
    }
}