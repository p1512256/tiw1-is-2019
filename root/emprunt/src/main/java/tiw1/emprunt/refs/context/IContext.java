package tiw1.emprunt.refs.context;

public interface IContext {
    public Object get(String key);
    public void set(String key, Object ref) throws Exception;
}
