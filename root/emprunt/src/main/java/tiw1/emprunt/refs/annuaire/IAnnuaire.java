package tiw1.emprunt.refs.annuaire;

public interface IAnnuaire {
    public Object find(String key);
    public Object bind(String key, Object entry) throws Exception;
}