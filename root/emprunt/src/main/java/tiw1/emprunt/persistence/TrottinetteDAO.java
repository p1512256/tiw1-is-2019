package tiw1.emprunt.persistence;

import java.util.List;
import java.util.Optional;

import tiw1.emprunt.mvc.entity.Trottinette;
import tiw1.emprunt.pool.TrottinettePool;
import tiw1.emprunt.utils.observer.IObserver;

public class TrottinetteDAO implements DAO<Trottinette>, IObserver {
    private TrottinettePool trottinettesPool = null;

    @Override
    public Optional<Trottinette> get(long id) throws Exception {
        Trottinette t = trottinettesPool.findPooledInstance(id).getValue();
        return Optional.ofNullable(t);
    }

    @Override
    public List<Trottinette> getAll() {
        return trottinettesPool.getAll();
    }

    @Override
    public void save(Trottinette t) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Trottinette t) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Trottinette t) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleChanges(Object source, String updatedKey, Object newValue) {
        if (updatedKey == "app/service/trottinettes") {
            this.trottinettesPool = (TrottinettePool) newValue;
        }
    }
}
