package tiw1.emprunt.pool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tiw1.emprunt.mvc.entity.Trottinette;
import tiw1.emprunt.utils.pool.manager.PoolManager;

public class TrottinettePool extends PoolManager<Long, Trottinette> {
    private Map<Long, Trottinette> trottinettes;
    private List<Trottinette> trottinettesList = null;

    public TrottinettePool(Map<Long, Trottinette> trottinettes) {
        this.trottinettes = trottinettes;
    }

    @Override
    public Trottinette find(Long key) {
        return trottinettes.get(key);
    }

    public List<Trottinette> getAll() {
        if (trottinettesList == null) trottinettesList = new ArrayList<Trottinette>(trottinettes.values());
        return trottinettesList;
    }
}