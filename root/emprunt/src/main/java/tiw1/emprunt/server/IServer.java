package tiw1.emprunt.server;

import java.util.Map;

public interface IServer {
    public Object processRequest(String method, String resource, String command, Map<String, Object> parameters) throws Exception;
}
