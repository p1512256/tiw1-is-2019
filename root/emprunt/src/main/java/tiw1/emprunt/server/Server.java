package tiw1.emprunt.server;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoBuilder;
import org.picocontainer.parameters.ConstantParameter;

import tiw1.emprunt.refs.annuaire.Annuaire;
import tiw1.emprunt.refs.annuaire.IAnnuaire;
import tiw1.emprunt.refs.context.Context;
import tiw1.emprunt.mvc.controller.AbonneController;
import tiw1.emprunt.mvc.controller.Controller;
import tiw1.emprunt.mvc.controller.EmpruntController;
import tiw1.emprunt.mvc.controller.TrottinetteController;
import tiw1.emprunt.mvc.entity.IEntity;
import tiw1.emprunt.persistence.AbonneDAO;
import tiw1.emprunt.persistence.EmpruntDAO;
import tiw1.emprunt.persistence.TrottinetteDAO;
import tiw1.emprunt.persistence.TrottinetteLoader;
import tiw1.emprunt.pool.TrottinettePool;
import tiw1.emprunt.mvc.model.AbonneResource;
import tiw1.emprunt.mvc.model.EmpruntResource;
import tiw1.emprunt.mvc.model.TrottinetteResource;
import tiw1.emprunt.utils.observer.Observable;

public class Server implements IServer {
    private MutablePicoContainer picoContainer;

    private JSONObject CONFIG = null;

    private String COMPANY_NAME = "";

    private IAnnuaire annuaire = null;

    protected static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    /* -------------- Constructor -------------- */

    public Server() throws IOException, Exception {
        loadConfig("config.json");

        TrottinetteLoader.load("http://localhost:8080/trottinette/");

        initContainer();

        initComponents();

        initAnnuaire();

        startContainer();
    }

    /* -------------- Methods -------------- */

    @Override
    public Object processRequest(String method, String resource, String command, Map<String, Object> parameters) throws Exception {
        @SuppressWarnings("unchecked")
        Controller<IEntity> controller = (Controller<IEntity>) annuaire.find("app/business/" + resource);
        return controller.process(method, command, parameters);
    }

    /* -------------- Init methods -------------- */

    private void loadConfig(String configPath) throws FileNotFoundException, IOException, ParseException {
        JSONParser parser = new JSONParser();

        CONFIG = (JSONObject) parser.parse(new FileReader(configPath));
        COMPANY_NAME = (String) ((JSONObject) CONFIG.get("app")).get("name");
    }

    private void initContainer() {
        this.picoContainer = new PicoBuilder()
            .withCaching()
            .withLifecycle()
            .build();
    }

    private void initComponents() throws Exception {
        picoContainer.addComponent((String) ((JSONObject) CONFIG.get("app")).get("name"));

        picoContainer.addComponent("trottinettes", TrottinetteLoader.getTrottinettes());
        picoContainer.addComponent(TrottinettePool.class);

        picoContainer.addComponent(Annuaire.class);
        picoContainer.addComponent(Context.class);

        picoContainer.addComponent(AbonneDAO.class, AbonneDAO.class, new ConstantParameter("abonnes.json"));
        picoContainer.addComponent(TrottinetteDAO.class);
        picoContainer.addComponent(EmpruntDAO.class);

        picoContainer.addComponent(AbonneResource.class);
        picoContainer.addComponent(AbonneController.class);

        picoContainer.addComponent(TrottinetteResource.class);
        picoContainer.addComponent(TrottinetteController.class);

        picoContainer.addComponent(EmpruntResource.class);
        picoContainer.addComponent(EmpruntController.class);
    }

    private void initAnnuaire() throws Exception {
        annuaire = picoContainer.getComponent(Annuaire.class);

        annuaire.bind("server", this);
        annuaire.bind("app/name", COMPANY_NAME);

        annuaire.bind("app/business/abonne", picoContainer.getComponent(AbonneController.class));
        annuaire.bind("app/business/trottinette", picoContainer.getComponent(TrottinetteController.class));
        annuaire.bind("app/business/emprunt", picoContainer.getComponent(EmpruntController.class));

        ((Observable) annuaire).addObserver(((AbonneController) annuaire.find("app/business/abonne")).getResource());
        ((Observable) annuaire).addObserver(((TrottinetteController) annuaire.find("app/business/trottinette")).getResource());
        ((Observable) annuaire).addObserver(((EmpruntController) annuaire.find("app/business/emprunt")).getResource());

        AbonneDAO abonneDAO = picoContainer.getComponent(AbonneDAO.class);
        TrottinetteDAO trottinetteDAO = picoContainer.getComponent(TrottinetteDAO.class);
        EmpruntDAO empruntDAO = picoContainer.getComponent(EmpruntDAO.class);

        annuaire.bind("app/persistence/abonne", abonneDAO);
        annuaire.bind("app/persistence/trottinette", trottinetteDAO);
        annuaire.bind("app/persistence/emprunt", empruntDAO);

        ((Observable) annuaire).addObserver((TrottinetteDAO) annuaire.find("app/persistence/trottinette"));

        annuaire.bind("app/service/trottinettes", picoContainer.getComponent(TrottinettePool.class));

        LOGGER.info(annuaire.toString());
    }

    private void startContainer() {
        picoContainer.start();
    }
}
