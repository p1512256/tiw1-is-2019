package tiw1.emprunt.utils.observer;

public interface IObservable {
    public void addObserver(IObserver observer);
    public void removeObserver(IObserver observer);

    public void notifyObservers(Object newValue, String updatedKey);
}