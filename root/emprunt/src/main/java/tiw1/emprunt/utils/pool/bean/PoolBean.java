package tiw1.emprunt.utils.pool.bean;

import tiw1.emprunt.utils.pool.manager.IPoolManager;

public class PoolBean<K, V> implements IPoolBean<K, V> {
	private IPoolManager<K, V> manager;

	private K key;
	private V value;
	private boolean isActive = false;

	public PoolBean(K key, V value, IPoolManager<K, V> manager) {
		this.key = key;
		this.value = value;
		this.manager = manager;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}

	@Override
	public void release() {
		manager.release(this);
	}

	@Override
	public void passivate() {
		this.isActive = false;
	}

	@Override
	public void activate() {
		this.isActive = true;
	}

	@Override
	public boolean isActive() {
		return isActive;
	}
}