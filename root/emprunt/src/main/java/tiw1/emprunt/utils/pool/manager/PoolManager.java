package tiw1.emprunt.utils.pool.manager;

import java.util.HashMap;
import java.util.Map;

import tiw1.emprunt.utils.pool.bean.IPoolBean;
import tiw1.emprunt.utils.pool.bean.PoolBean;

public abstract class PoolManager<K, V> implements IPoolManager<K, V> {
    private Map<K, IPoolBean<K, V>> pool = new HashMap<>();
    private final int MAX_INSTANCE_COUNT = 100;

    @Override
    public final IPoolBean<K, V> findPooledInstance(K key) throws Exception {
        IPoolBean<K, V> bean = pool.get(key);
        if (bean == null) {
            if (isPoolFull()) throw new Exception("Pool manager is full");
            V value = find(key);
            bean = poolInstance(key, value);
        }
        activate(key);
        return bean;
    }

    @Override
    public final boolean isPoolFull() {
        return pool.size() > MAX_INSTANCE_COUNT;
    }

    @Override
    public void release(IPoolBean<K, V> bean) {
        passivate(bean.getKey());
        // pool.put(bean.getKey(), bean);
    }

    private void passivate(K key) {
        pool.get(key).passivate();
    }
    private void activate(K key) {
        pool.get(key).activate();
    }

    @Override
    public IPoolBean<K, V> poolInstance(K key, V value) {
        IPoolBean<K, V> bean = new PoolBean<K, V>(key, value, this);
        pool.put(key, bean);
        return bean;
    }
}