package tiw1.emprunt.utils.pool.bean;

public interface IPoolBean<K, V> {
    public K getKey();
    public V getValue();
    public void setValue(V value);
    public void release();
    public void passivate();
    public void activate();
    public boolean isActive();
}