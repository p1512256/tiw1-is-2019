package tiw1.emprunt.utils.observer;

public interface IObserver {
    public void handleChanges(Object source, String updatedKey, Object newValue);
}