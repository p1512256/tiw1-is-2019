package tiw1.emprunt.utils.pool.manager;

import tiw1.emprunt.utils.pool.bean.IPoolBean;

public interface IPoolManager<K, V> {
    public IPoolBean<K, V> findPooledInstance(K key) throws Exception;

    // Tout l'intérêt du pattern Object Pool réside dans la complexité de l'opération find
    public V find(K key);
    public IPoolBean<K, V> poolInstance(K key, V value);
    public boolean isPoolFull();

    public void release(IPoolBean<K, V> bean);
}