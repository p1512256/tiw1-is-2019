package tiw1.emprunt.utils.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable implements IObservable {

    private List<IObserver> observers = new ArrayList<>();

    @Override
    public final void addObserver(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public final void removeObserver(IObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Object newValue, String updatedKey) {
        for (IObserver observer : observers) {
            observer.handleChanges(this, updatedKey, newValue);
        }
    }
}