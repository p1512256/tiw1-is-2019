package tiw1.emprunt.mvc.controller;

import tiw1.emprunt.mvc.entity.Trottinette;
import tiw1.emprunt.mvc.model.TrottinetteResource;

public class TrottinetteController extends Controller<Trottinette> {

    public TrottinetteController(TrottinetteResource resource) {
		super(resource);
	}
}
