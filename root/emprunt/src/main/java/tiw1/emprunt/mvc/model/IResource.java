package tiw1.emprunt.mvc.model;

import java.util.Map;

public interface IResource {
	public Object get(String command, Map<String, Object> parameters) throws Exception;
	public Object post(String command, Map<String, Object> parameters) throws Exception;
	public Object put(String command, Map<String, Object> parameters) throws Exception;
	public Object delete(String command, Map<String, Object> parameters) throws Exception;
}
