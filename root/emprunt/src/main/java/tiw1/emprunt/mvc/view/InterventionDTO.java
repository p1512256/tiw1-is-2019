package tiw1.emprunt.mvc.view;

import java.util.Date;

import tiw1.emprunt.mvc.entity.Intervention;

public class InterventionDTO implements DTO<Intervention> {
    private Long id;
    private Date date;
    private String description;

    public InterventionDTO(Long id, Date date, String description) {
        this.id = id;
        this.date = date;
        this.description = description;
    }

    public Long getId() {
        return this.id;
    }

    public Date getDate() {
        return this.date;
    }

    public String getDescription() {
        return this.description;
    }
}
