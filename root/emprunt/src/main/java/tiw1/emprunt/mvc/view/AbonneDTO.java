package tiw1.emprunt.mvc.view;

import java.util.Date;

import tiw1.emprunt.mvc.entity.Abonne;

public class AbonneDTO implements DTO<Abonne> {
    private Long id;
    private Date dateDebut;
    private Date dateFin;
    private String name;

    public AbonneDTO(Long id, Date dateDebut, Date dateFin, String name) {
        this.id = id;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public Date getDateDebut() {
        return this.dateDebut;
    }

    public Date getDateFin() {
        return this.dateFin;
    }

    public String getName() {
        return this.name;
    }
}
