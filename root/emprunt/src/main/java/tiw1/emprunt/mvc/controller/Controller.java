package tiw1.emprunt.mvc.controller;

import java.util.Map;
import java.util.logging.Logger;

import org.picocontainer.Startable;

import tiw1.emprunt.mvc.model.Resource;

public abstract class Controller<T> implements IController, Startable {

	private final Resource<T> resource;

	protected static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

	public Controller(Resource<T> resource) {
		this.resource = resource;
	}

	public Resource<T> getResource() {
		return this.resource;
	}

	@Override
	public void start() {
		LOGGER.info("Composant " + this.getClass().getName() + " démarré.");
	}

	@Override
	public void stop() {
		LOGGER.info("Composant " + this.getClass().getName() + " arrêté.");
	}

	@Override
	public Object process(String method, String command, Map<String, Object> parameters) throws Exception {
		/*
		Method requestedMethod = this.getClass().getMethod(method.toLowerCase());
		Object result = requestedMethod.invoke(resource, command, parameters);
		return result;
		 */

		switch (method.toUpperCase()) {
			case "GET":
				return resource.get(command, parameters);
			case "POST":
				return resource.post(command, parameters);
			case "PUT":
				return resource.put(command, parameters);
			case "DELETE":
				return resource.delete(command, parameters);
			default:
				return null;
		}
	}
}
