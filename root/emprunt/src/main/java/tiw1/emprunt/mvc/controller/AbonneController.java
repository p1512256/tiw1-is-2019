package tiw1.emprunt.mvc.controller;

import tiw1.emprunt.mvc.entity.Abonne;
import tiw1.emprunt.mvc.model.AbonneResource;

public class AbonneController extends Controller<Abonne> {

	public AbonneController(AbonneResource resource) {
		super(resource);
	}
}
