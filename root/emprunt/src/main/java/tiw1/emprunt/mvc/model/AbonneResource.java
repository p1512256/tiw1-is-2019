package tiw1.emprunt.mvc.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import tiw1.emprunt.refs.annuaire.IAnnuaire;
import tiw1.emprunt.refs.context.Context;
import tiw1.emprunt.mvc.entity.Abonne;
import tiw1.emprunt.persistence.AbonneDAO;

public class AbonneResource extends Resource<Abonne> {

	public AbonneResource(Context context) {
		super(context);
	}

	@Override
	public void start() {
		// setDAO((AbonneDAO) context.get(AbonneDAO.class.getName()));

		super.start();
	}

	@Override
	public void stop() {
		super.stop();
	}

	@Override
	public void handleChanges(Object source, String updatedKey, Object newValue) {
		super.handleChanges(source, updatedKey, newValue);
		if (updatedKey == "app/persistence/abonne") {
			setDAO((AbonneDAO) ((IAnnuaire) source).find(updatedKey));
		}
	}

	/* ---------------- Request methods ---------------- */

	@Override
	public Object get(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "get":
				return handleGetAbonne(parameters);
			case "getAll":
				return handleGetAllAbonne(parameters);
			default:
				return null;
		}
	}

	@Override
	public Object post(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "update":
				return handleUpdateAbonne(parameters);
			case "abonnement":
				return handleAbonnement(parameters);
			case "desabonnement":
				return handleDesabonnement(parameters);
			default:
				return null;
		}
	}

	@Override
	public Object put(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "create":
				return handleCreateAbonne(parameters);
			default:
				return null;
		}
	}

	@Override
	public Object delete(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "delete":
				return handleDeleteAbonne(parameters);
			default:
				return null;
		}
	}

	/* ---------------- Handler methods ---------------- */

	private Object handleGetAbonne(Map<String, Object> parameters) throws Exception {
		return getAbonne((Long) parameters.get("id"));
	}

	private Object handleGetAllAbonne(Map<String, Object> parameters) {
		return getAllAbonne();
	}

	private Object handleUpdateAbonne(Map<String, Object> parameters) throws Exception {
		return updateAbonne((Abonne) parameters.get("abonne"));
	}

	private Object handleCreateAbonne(Map<String, Object> parameters) throws Exception {
		if (parameters.get("abonne") != null) {
			return createAbonne((Abonne) parameters.get("abonne"));
		} else {
			return createAbonne(
				(Long) parameters.get("id"),
				(String) parameters.get("name"),
				(Date) parameters.get("dateDebut"),
				(Date) parameters.get("dateFin")
			);
		}
	}

	private Object handleDeleteAbonne(Map<String, Object> parameters) throws Exception {
		deleteAbonne((Abonne) parameters.get("abonne"));
		return null;
	}

	private Object handleAbonnement(Map<String, Object> parameters) throws Exception {
		abonnement((Long) parameters.get("id"));
		return null;
	}

	private Object handleDesabonnement(Map<String, Object> parameters) throws Exception {
		desabonnement((Long) parameters.get("id"));
		return null;
	}

	/* ---------------- Business logic methods ---------------- */

	private Abonne getAbonne(long idAbonne) throws Exception {
		return (Abonne) getDAO().get(idAbonne).orElse(null);
	}

	private List<Abonne> getAllAbonne() {
		return getDAO().getAll();
	}

	private Abonne updateAbonne(Abonne abonne) throws Exception {
		getDAO().update(abonne);
		return abonne;
	}

	private Abonne createAbonne(Abonne abonne) throws Exception {
		getDAO().save(abonne);
		return abonne;
	}

	private Abonne createAbonne(Long id, String name, Date dateDebut, Date dateFin) throws Exception {
		Abonne abonne = new Abonne();
		abonne.setId(id);
		abonne.setName(name);
		abonne.setDateDebut(dateDebut);
		abonne.setDateFin(dateFin);
		return createAbonne(abonne);
	}

	private void deleteAbonne(Abonne abonne) throws Exception {
		getDAO().delete(abonne);
	}

	private void abonnement(long idAbonne) throws Exception {
		Abonne abonne = getAbonne(idAbonne);
		if (abonne != null) {
			abonne.setDateDebut(new Date());
			updateAbonne(abonne);
		}
	}

	private void desabonnement(long idAbonne) throws Exception {
		Abonne abonne = getAbonne(idAbonne);
		if (abonne != null) {
			abonne.setDateFin(new Date());
			updateAbonne(abonne);
		}
	}
}
