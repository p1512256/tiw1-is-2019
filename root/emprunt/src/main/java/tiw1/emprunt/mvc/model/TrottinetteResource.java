package tiw1.emprunt.mvc.model;

import java.util.List;
import java.util.Map;

import javassist.NotFoundException;
import tiw1.emprunt.mvc.entity.Trottinette;
import tiw1.emprunt.persistence.TrottinetteDAO;
import tiw1.emprunt.refs.annuaire.IAnnuaire;
import tiw1.emprunt.refs.context.Context;

public class TrottinetteResource extends Resource<Trottinette> {

	public TrottinetteResource(Context context) {
		super(context);
	}

	@Override
	public void start() {
		// setDAO((TrottinetteDAO) context.get(TrottinetteDAO.class.getName()));

		super.start();
	}

	@Override
	public void stop() {
		super.stop();
	}

	@Override
	public void handleChanges(Object source, String updatedKey, Object newValue) {
		super.handleChanges(source, updatedKey, newValue);
		if (updatedKey == "app/persistence/trottinette") {
			setDAO((TrottinetteDAO) ((IAnnuaire) source).find(updatedKey));
		}
	}

	/* ---------------- Request methods ---------------- */

	@Override
	public Object get(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "getAll":
				return handleGetAllTrottinettes(parameters);
			case "get":
				return handleGetTrottinette(parameters);
			case "checkIfAvailable":
				return handleIsTrottinetteDisponible(parameters);
			default:
				return null;
		}
	}

	@Override
	public Object post(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "takeTrottinette":
				return handleTakeTrottinette(parameters);
			case "freeTrottinette":
				return handleFreeTrottinette(parameters);
			default:
				return null;
		}
	}

	/* ---------------- Handler methods ---------------- */

	private Object handleGetAllTrottinettes(Map<String, Object> parameters) {
		return getAllTrottinettes();
	}

	private Object handleGetTrottinette(Map<String, Object> parameters) throws Exception {
		return getTrottinette((Long) parameters.get("id"));
	}

	private Object handleIsTrottinetteDisponible(Map<String, Object> parameters) throws Exception {
		return isTrottinetteDisponible((Long) parameters.get("id"));
	}

	private Object handleTakeTrottinette(Map<String, Object> parameters) throws Exception {
		return takeTrottinette((Long) parameters.get("id"));
	}

	private Object handleFreeTrottinette(Map<String, Object> parameters) throws Exception {
		return freeTrottinette((Long) parameters.get("id"));
	}

	/* ---------------- Business logic methods ---------------- */

	public List<Trottinette> getAllTrottinettes() {
		return getDAO().getAll();
	}

	public Trottinette getTrottinette(long id) throws Exception {
		return getDAO().get(id).orElse(null);
	}

	public boolean isTrottinetteDisponible(long id) throws Exception {
		Trottinette t = getTrottinette(id);
		if (t == null) return false;
		return t.isDisponible();
	}

	public Trottinette takeTrottinette(long id) throws Exception {
		Trottinette t = getTrottinette(id);
		if (t == null) throw new NotFoundException("Trottinette inexistante");
		if (!t.isDisponible()) return null;
		t.setDisponible(false);
		return t;
	}

	public Trottinette freeTrottinette(long id) throws Exception {
		Trottinette t = getTrottinette(id);
		if (t == null) throw new NotFoundException("Trottinette inexistante");
		t.setDisponible(true);
		return t;
	}
}
