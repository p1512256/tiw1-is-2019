package tiw1.emprunt.mvc.controller;

import tiw1.emprunt.mvc.entity.Emprunt;
import tiw1.emprunt.mvc.model.EmpruntResource;

public class EmpruntController extends Controller<Emprunt> {

	public EmpruntController(EmpruntResource resource) {
		super(resource);
	}
}
