package tiw1.emprunt.mvc.view;

import java.util.List;

import tiw1.emprunt.mvc.entity.Intervention;
import tiw1.emprunt.mvc.entity.Trottinette;

public class TrottinetteDTO implements DTO<Trottinette> {
    private long id;
    private boolean disponible;
    private List<Intervention> interventions;

    public TrottinetteDTO(long id, boolean disponible, List<Intervention> interventions) {
        this.id = id;
        this.disponible = disponible;
        this.interventions = interventions;
    }

    public long getId() {
        return this.id;
    }

    public boolean getDisponible() {
        return this.disponible;
    }

    public boolean isDisponible() {
        return this.disponible;
    }

    public List<Intervention> getInterventions() {
        return this.interventions;
    }
}
