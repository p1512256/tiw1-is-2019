package tiw1.emprunt.mvc.model;

import java.util.Map;
import java.util.logging.Logger;

import org.picocontainer.Startable;

import tiw1.emprunt.refs.annuaire.Annuaire;
import tiw1.emprunt.refs.context.Context;
import tiw1.emprunt.persistence.DAO;
import tiw1.emprunt.utils.observer.IObserver;

public abstract class Resource<T> implements IResource, Startable, IObserver {

	protected Context context;
	protected DAO<T> dao;

	protected static final Logger LOGGER = Logger.getLogger(Resource.class.getName());

	public Resource(Context context) {
		this.context = context;
	}

	public DAO<T> getDAO() {
		return this.dao;
	}

	protected void setDAO(DAO<T> dao) {
		this.dao = dao;
	}

	@Override
	public void start() {
		LOGGER.info("Composant " + this.getClass().getName() + " démarré.");
		// LOGGER.info("Objet d'accès aux données: " + dao.toString());
	}

	@Override
	public void stop() {
		LOGGER.info("Composant " + this.getClass().getName() + " arrêté.");
	}

	@Override
	public Object get(String command, Map<String, Object> parameters) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object post(String command, Map<String, Object> parameters) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object put(String command, Map<String, Object> parameters) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object delete(String command, Map<String, Object> parameters) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public void handleChanges(Object source, String updatedKey, Object newValue) {
		if (updatedKey.contains("app/persistence")) {
			this.context = (Context) ((Annuaire) source).find("app/persistence");
		}
	}
}
