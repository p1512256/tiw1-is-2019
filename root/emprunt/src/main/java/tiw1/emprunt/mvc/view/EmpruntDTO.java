package tiw1.emprunt.mvc.view;

import java.util.Date;

import tiw1.emprunt.mvc.entity.Emprunt;

public class EmpruntDTO implements DTO<Emprunt> {
    private Long id;
    private Date date;
    private Long idAbonne, idTrottinette;

    public EmpruntDTO(Long id, Date date, Long idAbonne, Long idTrottinette) {
        this.id = id;
        this.date = date;
        this.idAbonne = idAbonne;
        this.idTrottinette = idTrottinette;
    }

    public Long getId() {
        return this.id;
    }

    public Date getDate() {
        return this.date;
    }

    public Long getIdAbonne() {
        return this.idAbonne;
    }

    public Long getIdTrottinette() {
        return this.idTrottinette;
    }

}
