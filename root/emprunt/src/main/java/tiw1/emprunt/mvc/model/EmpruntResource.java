package tiw1.emprunt.mvc.model;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import tiw1.emprunt.refs.annuaire.IAnnuaire;
import tiw1.emprunt.refs.context.Context;
import tiw1.emprunt.mvc.entity.Emprunt;
import tiw1.emprunt.persistence.EmpruntDAO;

public class EmpruntResource extends Resource<Emprunt> {

	public EmpruntResource(Context context) {
		super(context);
	}

	@Override
	public void start() {
		// setDAO((EmpruntDAO) context.get(EmpruntDAO.class.getName()));

		super.start();
	}

	@Override
	public void stop() {
		super.stop();
	}

	@Override
	public void handleChanges(Object source, String updatedKey, Object newValue) {
		super.handleChanges(source, updatedKey, newValue);
		if (updatedKey == "app/persistence/emprunt") {
			setDAO((EmpruntDAO) ((IAnnuaire) source).find(updatedKey));
		}
	}

	/* ---------------- Request methods ---------------- */

	@Override
	public Object get(String command, Map<String, Object> parameters) throws IOException {
		switch (command) {
			case "getEmpruntsBetween":
				return handleGetEmpruntsBetween(parameters);
			default:
				return null;
		}
	}

	@Override
	public Object put(String command, Map<String, Object> parameters) throws Exception {
		switch (command) {
			case "create":
				return handleCreateEmprunt(parameters);
			default:
				return null;
		}
	}

	/* ---------------- Handler methods ---------------- */

	private Object handleGetEmpruntsBetween(Map<String, Object> parameters) {
		return getEmpruntsBetween(
			(Date) parameters.get("startDate"),
			(Date) parameters.get("endDate")
		);
	}

	private Object handleCreateEmprunt(Map<String, Object> parameters) throws Exception {
		return createEmprunt(
			(Date) parameters.get("date"),
			(Long) parameters.get("idAbonne"),
			(Long) parameters.get("idTrottinette")
		);
	}

	/* ---------------- Business logic methods ---------------- */

	public Emprunt createEmprunt(Date date, Long idAbonne, Long idTrottinette) throws Exception {
		Emprunt emprunt = new Emprunt();
		emprunt.setDate(date);
		emprunt.setIdAbonne(idAbonne);
		emprunt.setIdTrottinette(idTrottinette);
		getDAO().save(emprunt);
		return emprunt;
	}

	public List<Emprunt> getEmpruntsBetween(Date startDate, Date endDate) {
		List<Emprunt> emprunts = getDAO().getAll();
		Predicate<Emprunt> outOfSearchRange = e -> e.getDate().before(startDate) || e.getDate().after(endDate);
		emprunts.removeIf(outOfSearchRange);
		return emprunts;
	}
}
