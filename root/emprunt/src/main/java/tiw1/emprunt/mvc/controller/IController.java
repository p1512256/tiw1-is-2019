package tiw1.emprunt.mvc.controller;

import java.util.Map;

public interface IController {
    public Object process(String method, String command, Map<String, Object> parameters) throws Exception;
}
