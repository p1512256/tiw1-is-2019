package tiw1.emprunt;

import org.junit.Before;
import org.junit.Test;

import tiw1.emprunt.mvc.entity.Trottinette;
import tiw1.emprunt.server.IServer;
import tiw1.emprunt.server.Server;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class TrottinetteLoaderTest {
    private IServer server = null;

    @Before
    public void initServer() throws IOException, Exception {
        server = new Server();
    }

    @Test
    public void testServerCreation() {
        System.out.println("testServerCreation");
        assertNotNull(server);
    }

    @Test
    public void testTrottinetteListCreation() {
        try {
            @SuppressWarnings("unchecked")
            List<Trottinette> ts = (List<Trottinette>) server.processRequest("GET", "trottinette", "getAll", null);
            assertNotNull(ts);
            System.out.println(ts.size() + " trottinettes trouvées");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            fail(ex.getMessage());
        }
    }
}
