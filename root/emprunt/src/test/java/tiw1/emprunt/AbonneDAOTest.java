package tiw1.emprunt;

import org.junit.Before;
import org.junit.Test;
import tiw1.emprunt.mvc.entity.Abonne;
import tiw1.emprunt.server.IServer;
import tiw1.emprunt.server.Server;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class AbonneDAOTest {

    private IServer server = null;

    @Before
    public void initServer() throws IOException, Exception {
        server = new Server();
    }

    @Test
    public void testServerCreation() {
        System.out.println("testServerCreation");
        assertNotNull(server);
    }

    @Test
    public void testAbonneCreation() throws Exception {
        System.out.println("testAbonneCreation");

        Abonne a = new Abonne(Long.valueOf(1), "Toto", new Date(), new Date());
        Map<String, Object> aMap = new HashMap<String, Object>();
        aMap.put("abonne", a);

        Abonne b = null;
        Map<String, Object> bMap = new HashMap<String, Object>();
        bMap.put("id", Long.valueOf(2));
        bMap.put("name", "Tata");
        bMap.put("dateDebut", new Date());
        bMap.put("dateFin", new Date());

        Abonne c = null;

        try {
            b = (Abonne) server.processRequest("PUT", "abonne", "create", aMap);
            c = (Abonne) server.processRequest("PUT", "abonne", "create", bMap);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        @SuppressWarnings("unchecked")
        List<Abonne> listAbonnes = (List<Abonne>) server.processRequest("GET", "abonne", "getAll", null);
        assertNotNull(listAbonnes);
        assertEquals(2, listAbonnes.size());
        assertNotNull(b);
        assertNotNull(c);
        assertEquals(a.getId(), b.getId());
    }

    @Test
    public void testAbonneSuppression() throws Exception {
        System.out.println("testAbonneSuppression");
        Abonne a = new Abonne(Long.valueOf(1), "Titi", new Date(), new Date());
        Map<String, Object> aMap = new HashMap<String, Object>();
        aMap.put("abonne", a);

        try {
            server.processRequest("DELETE", "abonne", "delete", aMap);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        @SuppressWarnings("unchecked")
        List<Abonne> listAbonnes = (List<Abonne>) server.processRequest("GET", "abonne", "getAll", null);
        assertNotNull(listAbonnes);
        assertEquals(1, listAbonnes.size());
    }

    @Test
    public void testAbonneUpdate() throws Exception {
        System.out.println("testAbonneUpdate");
        Abonne a = new Abonne(2L, "Tutu", new Date(), new Date());
        Map<String, Object> aMap = new HashMap<String, Object>();
        aMap.put("abonne", a);

        Abonne b = null;
        Map<String, Object> bMap = new HashMap<String, Object>();
        bMap.put("id", Long.valueOf(2));

        Abonne c = null;

        try {
            c = (Abonne) server.processRequest("POST", "abonne", "update", aMap);
            b = (Abonne) server.processRequest("GET", "abonne", "get", bMap);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        assertNotNull(b);
        assertNotNull(c);
        assertEquals(a, b);
    }
}
