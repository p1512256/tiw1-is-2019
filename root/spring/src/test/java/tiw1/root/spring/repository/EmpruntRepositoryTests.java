package tiw1.root.spring.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tiw1.root.spring.entity.Emprunt;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class EmpruntRepositoryTests {

    @Autowired
    private EmpruntRepository empruntRepository;
    private static Logger logger = Logger.getLogger(EmpruntRepositoryTests.class.getName());

    @Test
    void create() {
        Long initialCount = empruntRepository.count();
        Emprunt createdEmprunt = empruntRepository.save(new Emprunt(new Date(), null, null));
        Long countAfterCreate = empruntRepository.count();

        assertThat(countAfterCreate).isEqualTo(initialCount + 1);
        logger.info("createdEmprunt -> " + createdEmprunt);
    }

    @Test
    void findAll() {
        List<Emprunt> allEmprunts = empruntRepository.findAll();

        assertThat(allEmprunts).isNotNull();
        logger.info("allEmprunts -> " + allEmprunts.toString());
    }
}