package tiw1.root.spring.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tiw1.root.spring.entity.Emprunt;
import tiw1.root.spring.repository.EmpruntRepository;

import java.util.Date;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EmpruntServiceTests {

    @Autowired
    private EmpruntRepository empruntRepository;
    @Autowired
    private EmpruntService empruntService;
    private static Logger logger = Logger.getLogger(EmpruntServiceTests.class.getName());

    @BeforeEach
    void setUp() {
        empruntRepository.save(new Emprunt(new Date(), null, null));
    }

    @AfterEach
    void tearDown() {
        empruntRepository.deleteAll();
    }

    @Test
    void createGetEmprunt() {
        Long initialCount = empruntRepository.count();
        Emprunt emprunt = empruntService.createEmprunt(null, null);

        logger.info("created " + emprunt.toString());
        assertThat(emprunt).isNotNull();
        assertThat(empruntRepository.count()).isEqualTo(initialCount + 1);

        Emprunt emprunt1 = empruntService.getEmprunt(emprunt.getId()).orElse(null);

        logger.info("get " + emprunt.toString());
        assertThat(emprunt1).isNotNull();
        assertThat(emprunt1.getId()).isEqualTo(emprunt.getId());
    }

    @Test
    void activate() {
        Emprunt emprunt = empruntRepository.save(new Emprunt(new Date(), null, null));

        logger.info("created " + emprunt.toString());

        Emprunt activeEmprunt = empruntService.activateEmprunt(emprunt.getId());
        assertThat(activeEmprunt).isNotNull();
        assertThat(activeEmprunt.isActif()).isTrue();
    }
}