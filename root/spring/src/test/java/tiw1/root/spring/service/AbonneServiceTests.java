package tiw1.root.spring.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tiw1.root.spring.entity.Abonne;
import tiw1.root.spring.repository.AbonneRepository;

import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AbonneServiceTests {

    @Autowired
    private AbonneRepository abonneRepository;
    @Autowired
    private AbonneService abonneService;
    private static Logger logger = Logger.getLogger(AbonneServiceTests.class.getName());

    @BeforeEach
    void setUp() {
        abonneRepository.save(new Abonne("Randy"));
        logger.info("setUp() -> " + abonneRepository.count() + " abonné(s)");
    }

    @AfterEach
    void tearDown() {
        abonneRepository.deleteAll();
        logger.info("tearDown() -> " + abonneRepository.count() + " abonné(s)");
    }

    @Test
    void createAbonne() {
        Long initialCount = abonneRepository.count();
        Abonne createdAbonne = abonneService.createAbonne(new Abonne("Juliette"));
        logger.info("created " + createdAbonne.toString());

        assertThat(createdAbonne).isNotNull();
        assertThat(abonneRepository.count()).isEqualTo(initialCount + 1);
    }

    @Test
    void getAbonne() {
        Abonne abonne = abonneRepository.save(new Abonne("Juliette"));
        Abonne foundAbonne = abonneService.getAbonne(abonne.getId()).orElse(null);

        logger.info("abonne -> " + abonne.toString());
        logger.info("foundAbonne -> " + foundAbonne.toString());

        assertThat(foundAbonne).isNotNull();
        assertThat(foundAbonne).isEqualTo(abonne);
    }

    @Test
    void getAllAbonne() {
        abonneRepository.save(new Abonne("Juliette"));
        List<Abonne> allAbonnes = abonneService.getAllAbonne();

        logger.info("allAbonnes -> " + allAbonnes.toString());

        assertThat(allAbonnes).isNotNull();
        assertThat(allAbonnes.size()).isEqualTo(2);
    }

    @Test
    void updateAbonne() {
        Abonne abonne = abonneRepository.save(new Abonne("Juliette"));
        abonne.setName("Julien");
        Abonne updatedAbonne = abonneService.updateAbonne(abonne);

        logger.info("updatedAbonne -> " + updatedAbonne);

        assertThat(updatedAbonne).isNotNull();
        assertThat(updatedAbonne).isEqualTo(abonne);
    }

    @Test
    void abonnementDesabonnement() {
        Abonne abonne = abonneRepository.save(new Abonne("Juliette"));

        logger.info("abonne -> " + abonne.toString());
        assertThat(abonne.getDateDebut()).isNull();
        assertThat(abonne.getDateFin()).isNull();

        Abonne afterAbonnement = abonneService.abonnement(abonne.getId());

        logger.info("abonne -> " + afterAbonnement.toString());
        assertThat(afterAbonnement.getDateDebut()).isNotNull();
        assertThat(afterAbonnement.getDateFin()).isNull();

        Abonne afterDesabonnement = abonneService.desabonnement(abonne.getId());

        logger.info("abonne -> " + afterDesabonnement.toString());
        assertThat(afterDesabonnement.getDateDebut()).isNotNull();
        assertThat(afterDesabonnement.getDateFin()).isNotNull();
    }

    @Test
    void deleteAbonne() {
        Abonne abonne = abonneRepository.save(new Abonne("Juliette"));

        logger.info("abonne -> " + abonne.toString());

        Abonne beforeDelete = abonneService.getAbonne(abonne.getId()).orElse(null);
        assertThat(beforeDelete).isNotNull();

        abonneService.deleteAbonne(abonne);
        Abonne afterDelete = abonneService.getAbonne(abonne.getId()).orElse(null);
        assertThat(afterDelete).isNull();
    }
}