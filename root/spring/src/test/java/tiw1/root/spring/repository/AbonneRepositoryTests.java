package tiw1.root.spring.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tiw1.root.spring.entity.Abonne;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AbonneRepositoryTests {

    @Autowired
    private AbonneRepository abonneRepository;
    private static Logger logger = Logger.getLogger(AbonneRepositoryTests.class.getName());

    @Test
    void testCrudAbonne() {
        logger.info("\n[TEST] testCrudAbonne ===================================================");

        List<Abonne> allAbonnes = abonneRepository.findAll();
        assertThat(allAbonnes).isNotNull();
        logger.info("allAbonnes -> " + allAbonnes.toString());

        Long initialCount = abonneRepository.count();
        Abonne createdAbonne = abonneRepository.save(new Abonne("Randy"));
        Long countAfterCreate = abonneRepository.count();

        logger.info("createdAbonne -> " + createdAbonne.toString());
        assertThat(countAfterCreate).isEqualTo(initialCount + 1);

        allAbonnes = abonneRepository.findAll();
        assertThat(allAbonnes).isNotNull();
        logger.info("allAbonnes -> " + allAbonnes.toString());

        createdAbonne.setDateDebut(new Date());
        abonneRepository.save(createdAbonne);
        Abonne updatedAbonne = abonneRepository.findById(createdAbonne.getId()).orElse(null);
        Long countAfterUpdate = abonneRepository.count();

        allAbonnes = abonneRepository.findAll();
        assertThat(allAbonnes).isNotNull();
        logger.info("allAbonnes -> " + allAbonnes.toString());

        logger.info("createdAbonne -> " + createdAbonne.toString());
        logger.info("updatedAbonne -> " + updatedAbonne.toString());
        assertThat(updatedAbonne).isNotNull();
        assertThat(updatedAbonne != createdAbonne).isTrue();
        assertThat(updatedAbonne.getId()).isEqualTo(createdAbonne.getId());
        assertThat(countAfterUpdate).isEqualTo(countAfterCreate);

        abonneRepository.delete(updatedAbonne);
        Long countAfterDelete = abonneRepository.count();
        Abonne abonne = abonneRepository.findById(updatedAbonne.getId()).orElse(null);

        allAbonnes = abonneRepository.findAll();
        assertThat(allAbonnes).isNotNull();
        logger.info("allAbonnes -> " + allAbonnes.toString());

        logger.info("abonne -> " + abonne);
        assertThat(abonne).isNull();
        assertThat(countAfterDelete).isEqualTo(initialCount);
    }
}
