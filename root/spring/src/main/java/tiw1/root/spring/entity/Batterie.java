package tiw1.root.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.GeneratedValue;

@Entity
public class Batterie {

    @Id
    @GeneratedValue
    private Long id;
    private Boolean charging;
    private Double chargeLevel;

    public Batterie() {
        this(null, false, 0.0);
    }

    @JsonCreator
    public Batterie(
        @JsonProperty("id") Long id,
        @JsonProperty("charging") Boolean charging,
        @JsonProperty("chargeLevel") Double chargeLevel
    ) {
        this.id = id;
        this.charging = charging;
        this.chargeLevel = chargeLevel;
    }

    public Long getId() {
        return id;
    }
    public Batterie setId(Long id) {
        this.id = id;
        return this;
    }

    public Boolean isCharging() {
        return charging;
    }
    public Batterie setCharging(Boolean charging) {
        this.charging = charging;
        return this;
    }

    public Double getChargeLevel() {
        return chargeLevel;
    }
    public Batterie setChargeLevel(Double chargeLevel) {
        this.chargeLevel = chargeLevel;
        return this;
    }
}
