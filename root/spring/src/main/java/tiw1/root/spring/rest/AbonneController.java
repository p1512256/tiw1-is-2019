package tiw1.root.spring.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import tiw1.root.spring.entity.Abonne;
import tiw1.root.spring.service.AbonneService;

import java.util.Date;
import java.util.List;

@RestController
public class AbonneController {

    @Autowired
    private AbonneService abonneService;

    @PostMapping("/abonnes")
    public Abonne createAbonne(@RequestBody Abonne newAbonne) {
        abonneService.createAbonne(newAbonne);
        return newAbonne;
    }

    @GetMapping("/abonnes")
    public List<Abonne> getAllAbonnes() {
        return abonneService.getAllAbonne();
    }

    @GetMapping("/abonnes/{id}")
    public Abonne getAbonne(@PathVariable Long id) {
        return abonneService.getAbonne(id).orElse(null);
    }

    @PutMapping("/abonnes/{id}")
    public Abonne updateAbonne(
        @PathVariable Long id,
        @RequestBody Abonne updatedAbonne
    ) {
        Abonne abonne = abonneService.getAbonne(id).orElse(null);
        if (abonne == null) return null;
        abonne.setName(updatedAbonne.getName());
        abonneService.updateAbonne(abonne);
        return abonne;
    }

    @PutMapping("/abonnes/{id}/abonnement")
    public Abonne abonnement(@PathVariable Long id) {
        Abonne abonne = abonneService.getAbonne(id).orElse(null);
        if (abonne == null) return null;
        abonne.setDateDebut(new Date());
        abonne.setDateFin(null);
        abonneService.updateAbonne(abonne);
        return abonne;
    }

    @PutMapping("/abonnes/{id}/desabonnement")
    public Abonne desabonnement(@PathVariable Long id) {
        Abonne abonne = abonneService.getAbonne(id).orElse(null);
        if (abonne == null) return null;
        abonne.setDateFin(new Date());
        abonneService.updateAbonne(abonne);
        return abonne;
    }

    @DeleteMapping("/abonnes/{id}")
    public ResponseEntity<Object> deleteAbonne(@PathVariable Long id) {
        Abonne abonne = abonneService.getAbonne(id).orElse(null);
        if (abonne == null) return null;
        abonneService.deleteAbonne(abonne);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }
}
