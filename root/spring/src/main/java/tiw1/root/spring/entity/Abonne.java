package tiw1.root.spring.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Abonne {
    @Id
    @GeneratedValue
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date dateDebut;
    @Temporal(TemporalType.DATE)
    private Date dateFin;
    private String name;

    public Abonne() {
        this(null);
    }
    public Abonne(String name) {
        this(null, name, null, null);
    }
    public Abonne(
        Long id,
        String name,
        Date dateDebut
    ) {
        this(id, name, dateDebut, null);
    }
    @JsonCreator
    public Abonne(
        @JsonProperty("id") Long id,
        @JsonProperty("name") String name,
        @JsonProperty("dateDebut") Date dateDebut,
        @JsonProperty("dateFin") Date dateFin
    ) {
        this.id = id;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.name = name;
    }

    public Long getId() {
        return id;
    }
    public Abonne setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getDateDebut() {
        return dateDebut;
    }
    public Abonne setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public Date getDateFin() {
        return dateFin;
    }
    public Abonne setDateFin(Date dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public String getName() {
        return name;
    }
    public Abonne setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Abonne)) return false;
        Abonne abonne = (Abonne) o;
        return Objects.equals(getId(), abonne.getId()) &&
                Objects.equals(getDateDebut(), abonne.getDateDebut()) &&
                Objects.equals(getDateFin(), abonne.getDateFin()) &&
                Objects.equals(getName(), abonne.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDateDebut(), getDateFin(), getName());
    }

    @Override
    public String toString() {
        return "Abonne{" +
                "id=" + id +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", name='" + name + '\'' +
                '}';
    }
}
