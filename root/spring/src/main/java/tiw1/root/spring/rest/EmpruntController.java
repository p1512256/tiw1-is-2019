package tiw1.root.spring.rest;

import fr.univ_lyon1.tiw1_is.banque.service.BanquePortTypeService;
import fr.univ_lyon1.tiw1_is.banque.service.TransfertRequest;
import fr.univ_lyon1.tiw1_is.banque.service.TransfertResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tiw1.root.spring.entity.Activation;
import tiw1.root.spring.entity.Emprunt;
import tiw1.root.spring.service.ActivationService;
import tiw1.root.spring.service.EmpruntService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.xml.ws.WebServiceException;

@RestController
public class EmpruntController {

    @Autowired
    private EmpruntService empruntService;

    @Autowired
    private ActivationService activationService;

    private BanquePortTypeService banquePortTypeService;

    public EmpruntController() {
        URL wsdlLocation = null;
        WebServiceException e = null;
        try {
            wsdlLocation = new URL("file:/location/banque.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        banquePortTypeService = new BanquePortTypeService(wsdlLocation);
    }

    @PostMapping("/emprunts")
    public Emprunt createEmprunt(@RequestBody Emprunt emprunt) {
        Long idAbonne = emprunt.getIdAbonne();
        Long idTrottinette = emprunt.getIdTrottinette();
        return empruntService.createEmprunt(idAbonne, idTrottinette);
    }

    @GetMapping("/emprunts")
    public List<Emprunt> getEmprunts(
        @RequestParam(name = "startDate", required = false, defaultValue = "") String startEpoch,
        @RequestParam(name = "endDate", required = false, defaultValue = "") String endEpoch
    ) {
        if (startEpoch.isEmpty() && endEpoch.isEmpty()) return empruntService.getAllEmprunts();
        if (!startEpoch.isEmpty() && endEpoch.isEmpty()) return empruntService.getEmpruntsAfter(new Date(Long.parseLong(startEpoch)));
        if (!startEpoch.isEmpty() && !endEpoch.isEmpty()) return empruntService.getEmpruntsBetween(new Date(Long.parseLong(startEpoch)), new Date(Long.parseLong(endEpoch)));
        return empruntService.getAllEmprunts();
    }

    @GetMapping("/emprunts/{id}")
    public Emprunt getEmprunt(@PathVariable Long id) {
        return empruntService.getEmprunt(id).orElse(null);
    }

    @PostMapping("/emprunts/{id}/transfert")
    public Boolean requestTransfert(
        @PathVariable Long id,
        @RequestBody TransfertRequest transfertRequest
    ) {
        try {
            Emprunt emprunt = empruntService.getEmprunt(id).orElse(null);
            Activation activation = activationService.generateActivationNumber(emprunt.getId());
            transfertRequest.setActivation(activation.getId());
            transfertRequest.setMontant(EmpruntService.MONTANT_BASE);

            TransfertResponse transfertResponse = banquePortTypeService
                    .getBanquePortTypeSoap11()
                    .transfert(transfertRequest);

            return transfertResponse.isTransfertOk();
        } catch (Exception ex) {
            return false;
        }
    }
}
