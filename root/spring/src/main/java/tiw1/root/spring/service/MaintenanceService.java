package tiw1.root.spring.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tiw1.root.spring.entity.Trottinette;
import tiw1.root.spring.helper.EasyHttpClient;

@Component
public class MaintenanceService {

    @Autowired
    private EasyHttpClient httpClient;
    private String MAINTENANCE_API_URL;
    private static final String DEFAULT_MAINTENANCE_API_URL = "http://maintenance:8080";
    private static final Logger logger = Logger.getLogger(MaintenanceService.class.getName());

    public MaintenanceService() {
        this(DEFAULT_MAINTENANCE_API_URL);
    }
    public MaintenanceService(String MAINTENANCE_API_URL) {
        setMaintenanceUrl(MAINTENANCE_API_URL);
    }

    public String getMaintenanceUrl() {
        return MAINTENANCE_API_URL;
    }
    public MaintenanceService setMaintenanceUrl(String MAINTENANCE_API_URL) {
        this.MAINTENANCE_API_URL = MAINTENANCE_API_URL;
        return this;
    }

    public List<Trottinette> getAllTrottinettes() throws IOException {
        logger.info("getAllTrottinettes()");

        HttpGet getAllTrottinettesRequest = new HttpGet(MAINTENANCE_API_URL + "/trottinette");
        getAllTrottinettesRequest.addHeader("Content-type", "application/json");

        String response = httpClient.execute(getAllTrottinettesRequest);

        ObjectMapper mapper = new ObjectMapper();
        Trottinette[] trottinettesArray = mapper.readValue(response, Trottinette[].class);

        List<Trottinette> trottinettes = Arrays.asList(trottinettesArray);
        return trottinettes;
    }
    public Trottinette getTrottinette(Long id) throws IOException {
        logger.info("getTrottinette(" + id.toString() + ")");

        HttpGet getTrottinetteRequest = new HttpGet(MAINTENANCE_API_URL + "/trottinette/" + id.toString());
        getTrottinetteRequest.addHeader("Content-type", "application/json");

        String response = httpClient.execute(getTrottinetteRequest);

        ObjectMapper mapper = new ObjectMapper();
        Trottinette trottinette = mapper.readValue(response, Trottinette.class);

        return trottinette;
    }
    public Trottinette takeTrottinette(Long id) throws IOException {
        logger.info("takeTrottinette(" + id.toString() + ")");

        Trottinette trottinette = getTrottinette(id);

        if (trottinette != null) {
            trottinette.setDisponible(false);

            ObjectMapper mapper = new ObjectMapper();
            String body = mapper.writeValueAsString(trottinette);

            HttpPut updateTrottinetteRequest = new HttpPut(MAINTENANCE_API_URL + "/trottinette/" + id.toString());
            updateTrottinetteRequest.addHeader("Content-type", "application/json");
            updateTrottinetteRequest.setEntity(new StringEntity(body));

            String response = httpClient.execute(updateTrottinetteRequest);

            Trottinette updatedTrottinette = mapper.readValue(response, Trottinette.class);

            return updatedTrottinette;
        }

        return trottinette;
    }
    public Trottinette freeTrottinette(Long id) throws IOException {
        logger.info("takeTrottinette(" + id.toString() + ")");

        Trottinette trottinette = getTrottinette(id);

        if (trottinette != null) {
            trottinette.setDisponible(true);

            ObjectMapper mapper = new ObjectMapper();
            String body = mapper.writeValueAsString(trottinette);

            HttpPut updateTrottinetteRequest = new HttpPut(MAINTENANCE_API_URL + "/trottinette/" + id.toString());
            updateTrottinetteRequest.addHeader("Content-type", "application/json");
            updateTrottinetteRequest.setEntity(new StringEntity(body));

            String response = httpClient.execute(updateTrottinetteRequest);

            Trottinette updatedTrottinette = mapper.readValue(response, Trottinette.class);

            return updatedTrottinette;
        }

        return trottinette;
    }
}
