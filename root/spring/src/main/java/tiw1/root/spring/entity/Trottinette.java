package tiw1.root.spring.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import java.util.List;
import java.util.Objects;

@Entity
public class Trottinette {
    @Id
    @GeneratedValue
    private Long id;
    private boolean disponible;
    @OneToMany
    private List<Intervention> interventions;
    @OneToOne
    private Batterie batterie;

    public Trottinette() {
        this(null);
    }
    public Trottinette(Long id) {
        this(id, true);
    }
    public Trottinette(Long id, boolean disponible) {
        this(id, disponible, null);
    }
    @JsonCreator
    public Trottinette(
        @JsonProperty("id") Long id,
        @JsonProperty("disponible") boolean disponible,
        @JsonProperty("batterie") Batterie batterie
    ) {
        this.id = id;
        this.disponible = disponible;
        this.batterie = batterie;
    }

    public Long getId() {
        return id;
    }
    public Trottinette setId(Long id) {
        this.id = id;
        return this;
    }

    public boolean isDisponible() {
        return disponible;
    }
    public Trottinette setDisponible(boolean disponible) {
        this.disponible = disponible;
        return this;
    }

    public List<Intervention> getInterventions() {
        return interventions;
    }
    public Trottinette setInterventions(List<Intervention> interventions) {
        this.interventions = interventions;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trottinette)) return false;
        Trottinette that = (Trottinette) o;
        return isDisponible() == that.isDisponible() &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getInterventions(), that.getInterventions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), isDisponible(), getInterventions());
    }

    @Override
    public String toString() {
        return "Trottinette{" +
                "id=" + id +
                ", disponible=" + disponible +
                ", interventions=" + interventions +
                '}';
    }
}
