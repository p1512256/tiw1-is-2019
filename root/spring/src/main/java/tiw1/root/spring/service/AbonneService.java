package tiw1.root.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tiw1.root.spring.entity.Abonne;
import tiw1.root.spring.repository.AbonneRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class AbonneService {

    @Autowired
    private AbonneRepository abonneRepository;

    public Abonne createAbonne(Abonne abonne) {
        return abonneRepository.save(abonne);
    }

    public Optional<Abonne> getAbonne(Long id) {
        return abonneRepository.findById(id);
    }

    public List<Abonne> getAllAbonne() {
        return abonneRepository.findAll();
    }

    public Abonne updateAbonne(Abonne abonne) {
        return abonneRepository.save(abonne);
    }

    public Abonne abonnement(Long id) {
        Abonne abonne = abonneRepository.findById(id).orElse(null);
        if (abonne != null) {
            abonne.setDateDebut(new Date());
            abonneRepository.save(abonne);
        }
        return abonne;
    }

    public Abonne desabonnement(Long id) {
        Abonne abonne = abonneRepository.findById(id).orElse(null);
        if (abonne != null) {
            abonne.setDateFin(new Date());
            abonneRepository.save(abonne);
        }
        return abonne;
    }

    public void deleteAbonne(Abonne abonne) {
        abonneRepository.delete(abonne);
    }
}
