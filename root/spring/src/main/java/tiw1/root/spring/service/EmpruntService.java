package tiw1.root.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tiw1.root.spring.entity.Emprunt;
import tiw1.root.spring.repository.EmpruntRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class EmpruntService {

    public final static double MONTANT_BASE = 5.0;

    @Autowired
    private EmpruntRepository empruntRepository;

    public Emprunt createEmprunt(Long idAbonne, Long idTrottinette) {
        return empruntRepository.save(new Emprunt(idAbonne, idTrottinette));
    }

    public Emprunt createEmprunt(Date date, Long idAbonne, Long idTrottinette) {
        return empruntRepository.save(new Emprunt(date, idAbonne, idTrottinette));
    }

    public List<Emprunt> getEmpruntsBetween(Date startDate, Date endDate) {
        return empruntRepository.findByDateBetween(startDate, endDate);
    }

    public List<Emprunt> getAllEmprunts() {
        return empruntRepository.findAll();
    }

    public List<Emprunt> getEmpruntsAfter(Date startDate) {
        return empruntRepository.findByDateAfter(startDate);
    }

    public Optional<Emprunt> getEmprunt(Long id) {
        return empruntRepository.findById(id);
    }

    public Emprunt activateEmprunt(Long id) {
        Emprunt emprunt = getEmprunt(id).orElse(null);
        if (emprunt != null) {
            emprunt.setActif(true);
            empruntRepository.save(emprunt);
        }
        return emprunt;
    }
}
