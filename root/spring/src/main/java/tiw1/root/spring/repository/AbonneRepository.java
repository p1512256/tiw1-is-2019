package tiw1.root.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tiw1.root.spring.entity.Abonne;

import java.util.List;

@Repository
public interface AbonneRepository extends CrudRepository<Abonne, Long> {
    @Override
    List<Abonne> findAll();
}
