package tiw1.root.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tiw1.root.spring.entity.Emprunt;

import java.util.Date;
import java.util.List;

@Repository
public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {
    @Override
    List<Emprunt> findAll();
    List<Emprunt> findByDateBetween(Date startDate, Date endDate);
    List<Emprunt> findByDateAfter(Date startDate);
}
