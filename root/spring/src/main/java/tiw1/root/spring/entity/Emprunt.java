package tiw1.root.spring.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import tiw1.root.spring.service.EmpruntService;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Emprunt {
    @Id
    @GeneratedValue
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date date;
    private Long idAbonne, idTrottinette;
    private boolean actif;
    private double montant;

    public Emprunt() {
        this(null, null, null, null, false, EmpruntService.MONTANT_BASE);
    }
    public Emprunt(Long idAbonne, Long idTrottinette) {
        this(null, new Date(), idAbonne, idTrottinette, false, EmpruntService.MONTANT_BASE);
    }
    public Emprunt(Date date, Long idAbonne, Long idTrottinette) {
        this(null, date, idAbonne, idTrottinette, false, EmpruntService.MONTANT_BASE);
    }
    @JsonCreator
    public Emprunt(
        @JsonProperty("id") Long id,
        @JsonProperty("date") Date date,
        @JsonProperty("idAbonne") Long idAbonne,
        @JsonProperty("idTrottinette") Long idTrottinette,
        @JsonProperty("actif") boolean actif,
        @JsonProperty("montant") double montant
    ) {
        this.id = id;
        this.date = date;
        this.idAbonne = idAbonne;
        this.idTrottinette = idTrottinette;
        this.actif = actif;
        this.montant = montant;
    }

    public Long getId() {
        return id;
    }
    public Emprunt setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getDate() {
        return date;
    }
    public Emprunt setDate(Date date) {
        this.date = date;
        return this;
    }

    public Long getIdAbonne() {
        return idAbonne;
    }
    public Emprunt setIdAbonne(Long idAbonne) {
        this.idAbonne = idAbonne;
        return this;
    }

    public Long getIdTrottinette() {
        return idTrottinette;
    }
    public Emprunt setIdTrottinette(Long idTrottinette) {
        this.idTrottinette = idTrottinette;
        return this;
    }

    public boolean isActif() {
        return actif;
    }
    public Emprunt setActif(boolean actif) {
        this.actif = actif;
        return this;
    }

    public double getMontant() {
        return montant;
    }
    public Emprunt setMontant(double montant) {
        this.montant = montant;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Emprunt)) return false;
        Emprunt emprunt = (Emprunt) o;
        return isActif() == emprunt.isActif() &&
                Double.compare(emprunt.getMontant(), getMontant()) == 0 &&
                Objects.equals(getId(), emprunt.getId()) &&
                Objects.equals(getDate(), emprunt.getDate()) &&
                Objects.equals(getIdAbonne(), emprunt.getIdAbonne()) &&
                Objects.equals(getIdTrottinette(), emprunt.getIdTrottinette());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDate(), getIdAbonne(), getIdTrottinette(), isActif(), getMontant());
    }

    @Override
    public String toString() {
        return "Emprunt{" +
                "id=" + id +
                ", date=" + date +
                ", idAbonne=" + idAbonne +
                ", idTrottinette=" + idTrottinette +
                ", actif=" + actif +
                ", montant=" + montant +
                '}';
    }
}
