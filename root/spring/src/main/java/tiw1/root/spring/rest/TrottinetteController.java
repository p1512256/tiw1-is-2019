package tiw1.root.spring.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import tiw1.root.spring.entity.Trottinette;
import tiw1.root.spring.service.MaintenanceService;

import java.io.IOException;
import java.util.List;

@RestController
public class TrottinetteController {

    @Autowired
    private MaintenanceService maintenanceService;

    @GetMapping("/trottinettes")
    public List<Trottinette> getAllTrottinettes() throws Exception {
        return maintenanceService.getAllTrottinettes();
    }

    @GetMapping("/trottinettes/{id}")
    public Trottinette getTrottinette(@PathVariable Long id) throws IOException {
        return maintenanceService.getTrottinette(id);
    }

    @GetMapping("/trottinettes/{id}/isDisponible")
    public Boolean isTrottinetteDisponible(@PathVariable Long id) throws IOException {
        Trottinette trottinette = maintenanceService.getTrottinette(id);
        if (trottinette == null) return false;
        return trottinette.isDisponible();
    }

    @PostMapping("/trottinettes/{id}/takeTrottinette")
    public Trottinette takeTrottinette(@PathVariable Long id) throws IOException {
        return maintenanceService.takeTrottinette(id);
    }

    @PostMapping("/trottinettes/{id}/freeTrottinette")
    public Trottinette freeTrottinette(@PathVariable Long id) throws IOException {
        return maintenanceService.freeTrottinette(id);
    }
}
