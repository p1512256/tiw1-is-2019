package tiw1.root.spring.soap;

import fr.univ_lyon1.tiw1_is.confirmation.service.ConfirmationRequest;
import fr.univ_lyon1.tiw1_is.confirmation.service.ConfirmationResponse;
import fr.univ_lyon1.tiw1_is.confirmation.service.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import tiw1.root.spring.entity.Activation;
import tiw1.root.spring.entity.Emprunt;
import tiw1.root.spring.service.ActivationService;

@Endpoint
public class ConfirmationEndpoint {
    public final static String NAMESPACE_URI = "http:/univ-lyon1.fr/tiw1-is/confirmation/service";

    @Autowired
    private ActivationService activationService;

    private final static ObjectFactory confirmationObjectFactory = new ObjectFactory();

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "confirmationRequest")
    @ResponsePayload
    public ConfirmationResponse confirmation(@RequestPayload ConfirmationRequest confirmationRequest) {
        ConfirmationResponse response = confirmationObjectFactory.createConfirmationResponse();
        try {
            Long idActivation = confirmationRequest.getActivation();
            Activation activation = activationService.getActivation(idActivation).orElse(null);
            Emprunt emprunt = activationService.activateEmprunt(activation.getIdEmprunt());
            response.setConfirmationOk(emprunt.isActif());
        } catch (Exception ex) {
            response.setConfirmationOk(false);
        }
        return response;
    }
}
