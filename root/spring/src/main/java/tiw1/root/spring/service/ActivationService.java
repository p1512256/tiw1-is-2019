package tiw1.root.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tiw1.root.spring.entity.Activation;
import tiw1.root.spring.entity.Emprunt;
import tiw1.root.spring.repository.ActivationRepository;

import java.util.Optional;

@Component
public class ActivationService {

    @Autowired
    private ActivationRepository activationRepository;

    @Autowired
    private EmpruntService empruntService;

    public Activation generateActivationNumber(Long idEmprunt) {
        return activationRepository.save(new Activation(idEmprunt));
    }

    public Optional<Activation> getActivation(Long idActivation) {
        return activationRepository.findById(idActivation);
    }

    public boolean checkEmpruntState(Long idEmprunt) {
        Activation activation = activationRepository.findByIdEmprunt(idEmprunt).orElse(null);
        if (activation == null) return false;
        Emprunt emprunt = empruntService.getEmprunt(activation.getIdEmprunt()).orElse(null);
        if (emprunt == null) return false;
        return emprunt.isActif();
    }

    public Emprunt activateEmprunt(Long idEmprunt) {
        return empruntService.activateEmprunt(idEmprunt);
    }
}
