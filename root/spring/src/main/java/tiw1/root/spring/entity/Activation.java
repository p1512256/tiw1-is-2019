package tiw1.root.spring.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Activation {
    @Id
    @GeneratedValue
    private Long id;
    private Long idEmprunt;

    public Activation() {
        this(null, null);
    }
    public Activation(Long idEmprunt) {
        this(null, idEmprunt);
    }
    @JsonCreator
    public Activation(
        @JsonProperty("id") Long id,
        @JsonProperty("idEmprunt") Long idEmprunt
    ) {
        this.id = id;
        this.idEmprunt = idEmprunt;
    }

    public Long getId() {
        return id;
    }
    public Activation setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getIdEmprunt() {
        return idEmprunt;
    }
    public Activation setIdEmprunt(Long idEmprunt) {
        this.idEmprunt = idEmprunt;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Activation)) return false;
        Activation that = (Activation) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getIdEmprunt(), that.getIdEmprunt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getIdEmprunt());
    }

    @Override
    public String toString() {
        return "Activation{" +
                "id=" + id +
                ", idEmprunt=" + idEmprunt +
                '}';
    }
}
