package tiw1.root.spring.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tiw1.root.spring.entity.Activation;
import tiw1.root.spring.service.ActivationService;

import java.util.logging.Logger;

@Component
public class MessageReceiver {

    @Autowired
    private ActivationService activationService;

    private static final Logger logger = Logger.getLogger(MessageReceiver.class.getName());

    public void receiveMessage(String message) {
        logger.info("Received <" + message + ">");

        try {
            Activation activation = activationService.getActivation(Long.valueOf(message)).orElse(null);
            activationService.activateEmprunt(activation.getIdEmprunt());
        } catch (Exception e) {}
    }
}
