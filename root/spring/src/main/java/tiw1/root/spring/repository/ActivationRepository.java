package tiw1.root.spring.repository;

import org.springframework.data.repository.CrudRepository;
import tiw1.root.spring.entity.Activation;

import java.util.Optional;

public interface ActivationRepository extends CrudRepository<Activation, Long> {
    Optional<Activation> findByIdEmprunt(Long idEmprunt);
}
