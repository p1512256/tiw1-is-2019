package tiw1.root.spring.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class Intervention {
    @Id
    @GeneratedValue
    private Long id;
    private Date date;
    private String description;

    public Intervention() {
        this(null, null, null);
    }
    @JsonCreator
    public Intervention(
        @JsonProperty("id") Long id,
        @JsonProperty("date") Date date,
        @JsonProperty("description") String description) {
        this.id = id;
        this.date = date;
        this.description = description;
    }

    public Long getId() {
        return id;
    }
    public Intervention setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getDate() {
        return date;
    }
    public Intervention setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getDescription() {
        return description;
    }
    public Intervention setDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Intervention)) return false;
        Intervention that = (Intervention) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDate(), getDescription());
    }

    @Override
    public String toString() {
        return "Intervention{" +
                "id=" + id +
                ", date=" + date +
                ", description='" + description + '\'' +
                '}';
    }
}
