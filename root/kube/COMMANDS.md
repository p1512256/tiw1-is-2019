# Orchestration avec Kubernetes

## Premiers déploiements

### Conteneurs stateless: `nginx`

```bash
kubectl apply -f <deployment.yaml>
kubectl describe deployment <deployment-name>
kubectl get pods -l <deployment-label>=<deployment-name>
kubectl describe pod <pod-name>
kubectl port-forward <pod-name> 3000:80
```

### Conteneurs stateful: postgresql

```bash
kubectl describe pvc <pvc-name>

# En local
kubectl port-forward <pod-postgres> 3000:5432
psql -h 127.0.0.1 -p 3000 -U POSTGRES_USER -W POSTGRES_PASSWORD -d POSTGRES_DB
```

## Déploiement des applications

```bash
kubectl --namespace=grp-21 create secret docker-registry p1512256-forge --docker-server=forge.univ-lyon1.fr:4567 --docker-username=p1512256 --docker-password=CV8xDm-rj3zax3kVxoBd --docker-email=randy.andriamaro@etu.univ-lyon1.fr

cd banque ; docker build --no-cache --build-arg http_proxy="http://proxy.univ-lyon1.fr:3128" -t forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/banque . ; docker push forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/banque ; cd ..
cd maintenance-web ; docker build --no-cache --build-arg http_proxy="http://proxy.univ-lyon1.fr:3128" -t forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/maintenance . ; docker push forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/maintenance ; cd ..
cd spring ; docker build --no-cache --build-arg http_proxy="http://proxy.univ-lyon1.fr:3128" -t forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/location . ; docker push forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/location ; cd ..
cd proxy ; docker build --no-cache --build-arg http_proxy="http://proxy.univ-lyon1.fr:3128" -t forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/proxy . ; docker push forge.univ-lyon1.fr:4567/p1512256/tiw1-is-2019/proxy ; cd ..

kubectl create configmap maintenance-db-config --from-file=maintenance-db/maintenance-db.properties --namespace=grp-21
```