# TIW1 TP

## URL Services

|     Service      | Port | Sous-domaine |
|------------------|------|--------------|
| Maintenance      | 8080 |              |
| Banque           | 8081 |              |
| Location         | 8082 |              |
| RabbitMQ         | 8083 |              |
| Banque DB        | 8084 |              |
| Maintenance DB   | 8085 |              |
| Location DB      | 8086 |              |
| RabbitMQ UI      | 9090 | rabbitmq-ui  |
| Adminer          | 9091 | adminer      |