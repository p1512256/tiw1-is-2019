#!/bin/bash

TIW1_ROOT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/../.."

function build_image_banque {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/banque/

    docker build -t banque .

    cd $previous_dir
}

function build_image_maintenance {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/maintenance-web/

    docker build -t maintenance .

    cd $previous_dir
}

function build_image_location {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/spring/

    docker build -t location .

    cd $previous_dir
}

function build_image_all {
    build_image_banque &
    build_image_maintenance &
    build_image_location &

    wait
    echo -e "\033[0;32m\xE2\xA6\xBF\033[0m  Build \033[0;32mcomplete\033[0m"
}