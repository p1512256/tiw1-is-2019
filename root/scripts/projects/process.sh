#!/bin/bash

TIW1_ROOT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/../.."

function run_banque {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/banque/

    echo "\n\n==================================================================== $(date) ====================================================================" >> $TIW1_ROOT_DIR/logs/banque.log
    echo "Executing banque[mvn spring-boot:run]"

    mvn spring-boot:run >> $TIW1_ROOT_DIR/logs/banque.log &
    BANQUE_PROCESS=$!

    cd $previous_dir
    return $BANQUE_PROCESS
}

function run_maintenance {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/maintenance-web/

    echo "\n\n==================================================================== $(date) ====================================================================" >> $TIW1_ROOT_DIR/logs/maintenance-web.log
    echo "Executing maintenance-web[mvn jetty:run]"

    mvn jetty:run >> $TIW1_ROOT_DIR/logs/maintenance-web.log &
    MAINTENANCE_PROCESS=$!

    cd $previous_dir
    return $MAINTENANCE_PROCESS
}

function run_location {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/spring/

    echo "\n\n==================================================================== $(date) ====================================================================" >> $TIW1_ROOT_DIR/logs/spring-emprunt.log
    echo "Executing spring[mvn spring-boot:run]"

    mvn spring-boot:run >> $TIW1_ROOT_DIR/logs/spring-emprunt.log &
    EMPRUNT_PROCESS=$!

    cd $previous_dir
    return $EMPRUNT_PROCESS
}

function stop_maintenance {
    kill $MAINTENANCE_PROCESS
}

function stop_banque {
    kill $BANQUE_PROCESS
}

function stop_location {
    kill $EMPRUNT_PROCESS
}


function run_all {
    run_banque
    run_location
    run_maintenance
    sleep 1
    follow_logs
}

function stop_all {
    stop_banque
    stop_location
    stop_maintenance
    sleep 3
    clear_db
    clear_logs
}

function clear_logs {
    rm -v $TIW1_ROOT_DIR/logs/*.log
}

function clear_db {
    rm -v ~/data/tiw1/*.db
}

function echo_process_ids {
    echo "BANQUE_PROCESS  \t $BANQUE_PROCESS"
    echo "MAINTENANCE_PROCESS  \t $MAINTENANCE_PROCESS"
    echo "EMPRUNT_PROCESS  \t $EMPRUNT_PROCESS"
}

function check_status {
    if ps -p $BANQUE_PROCESS > /dev/null
    then
        echo -e "\033[0;32m\xE2\xA6\xBF\033[0m  BANQUE_PROCESS \t [$BANQUE_PROCESS]:\t\033[0;32mRUNNING\033[0m"
    else
        echo -e "\033[0;31m\xE2\xA6\xBF\033[0m  BANQUE_PROCESS \t [$BANQUE_PROCESS]:\t\033[0;31mSTOPPED\033[0m"
    fi

    if ps -p $MAINTENANCE_PROCESS > /dev/null
    then
        echo -e "\033[0;32m\xE2\xA6\xBF\033[0m  MAINTENANCE_PROCESS \t [$MAINTENANCE_PROCESS]:\t\033[0;32mRUNNING\033[0m"
    else
        echo -e "\033[0;31m\xE2\xA6\xBF\033[0m  MAINTENANCE_PROCESS \t [$MAINTENANCE_PROCESS]:\t\033[0;31mSTOPPED\033[0m"
    fi

    if ps -p $EMPRUNT_PROCESS > /dev/null
    then
        echo -e "\033[0;32m\xE2\xA6\xBF\033[0m  EMPRUNT_PROCESS \t [$EMPRUNT_PROCESS]:\t\033[0;32mRUNNING\033[0m"
    else
        echo -e "\033[0;31m\xE2\xA6\xBF\033[0m  EMPRUNT_PROCESS \t [$EMPRUNT_PROCESS]:\t\033[0;31mSTOPPED\033[0m"
    fi
}

function follow_logs {
    tail -f logs/*.log
}