#!/bin/bash

TIW1_ROOT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/../.."

function rebuild_banque {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/banque/

    mvn -Dmaven.test.skip=true clean install package

    cd $previous_dir
}

function rebuild_maintenance {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/maintenance-web/

    mvn -Dmaven.test.skip=true clean package

    cd $previous_dir
}

function rebuild_location {
    local previous_dir=$(pwd)
    cd $TIW1_ROOT_DIR/spring/

    mvn -Dmaven.test.skip=true clean install package

    cd $previous_dir
}

function rebuild_all {
    rebuild_banque &
    rebuild_maintenance &
    rebuild_location &

    wait
    echo -e "\033[0;32m\xE2\xA6\xBF\033[0m  Build \033[0;32mcomplete\033[0m"
}