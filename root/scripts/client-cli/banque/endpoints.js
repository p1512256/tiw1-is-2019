const banqueService = require("./service")

async function createCompte(compte) {
	try {
		const response = await banqueService.post("/compte/", compte)
		return response.data
	} catch (err) { console.error(err) }
}

async function getAllComptes() {
	try {
		const response = await banqueService.get("/compte")
		return response.data
	} catch (err) { console.error(err) }
}

async function getCompte(id) {
	try {
		const response = await banqueService.get(`/compte/${id}`)
		return response.data
	} catch (err) { console.error(err) }
}

async function newAutorisation(id, autorisation) {
	try {
		const response = await banqueService.post(`/compte/${id}`, autorisation)
		return response.data
	} catch (err) { console.error(err) }
}

module.exports = {
	createCompte,
	getAllComptes,
	getCompte,
	newAutorisation,
}
