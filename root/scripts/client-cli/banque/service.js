const axios = require("axios").default
const { logRequest } = require("../utils")

/**
 * * La variable d'environnement BANQUE_API_URL peut être spécifiée
 * * dans le fichier .env ou au lancement du script npm start
 * * pour indiquer l'endpoint du service banque.
 *
 * * Valeur par défaut: http://localhost:9091
 */
const banqueApiURL = process.env.BANQUE_API_URL || "http://localhost:8081"

const banqueService = axios.create({
	baseURL: banqueApiURL,
	timeout: 0,
})
banqueService.interceptors.request.use(logRequest("service:banque"))

module.exports = banqueService
