require("dotenv").config()

const path = require("path")
const debug = require("debug")
const logger = debug("client:seed:banque")
const {
    createCompte,
    getAllComptes,
    newAutorisation,
} = require("./endpoints")
const { saveJsonToFile } = require("../utils")

async function seed() {
    logger("Peuplement de l'API Banque...")

    let comptes = await getAllComptes()

    if (comptes.length > 0) {
        logger("Il existe déjà des comptes: ")
    } else {
        const compte1 = await createCompte({ valeur: 1204.0 })
        const compte2 = await createCompte({ valeur: 695.0 })
        const compte3 = await createCompte({ valeur: -43.0 })
        const compte4 = await createCompte({ valeur: 50.0 })

        // const autorisation1 = await newAutorisation(compte1.id, { montant: 15.0 })
        // const autorisation2 = await newAutorisation(compte2.id, { montant: 34.0, used: true })
        // const autorisation3 = await newAutorisation(compte2.id, { montant: 5.0 })

        comptes = await getAllComptes()
    }
    logger('%O', comptes)

    const samplePath = path.join(__dirname, "banque-sample.json")
    saveJsonToFile(samplePath, { comptes })
}

seed()
