require("dotenv").config()

const debug = require("debug")

const maintenance = require("./maintenance-web/endpoints")
const banque = require("./banque/endpoints")
const location = require("./location/endpoints")

async function maintenance_test() {
    const logger = debug("client:test:maintenance-web")

    const batteries = await maintenance.batterie.getAllBatteries()
    logger('%O', "batteries: ")
    logger('%O', batteries)

    const newBatterie = await maintenance.batterie.addBatterie()
    logger('%O', "newBatterie: ")
    logger('%O', newBatterie)

    const batterie = await maintenance.batterie.getBatterieById(newBatterie.id)
    logger('%O', "batterie: ")
    logger('%O', batterie)
    batterie.charging = true
    batterie.chargeLevel = 5.5

    const updatedBatterie = await maintenance.batterie.updateBatterie(
        batterie.id,
        batterie
    )
    logger('%O', "updatedBatterie: ")
    logger('%O', updatedBatterie)

    const trottinettes = await maintenance.trottinette.getAllTrottinettes()
    logger('%O', "trottinettes: ")
    logger('%O', trottinettes)

    const newTrottinette = await maintenance.trottinette.addTrottinette()
    logger('%O', "newTrottinette: ")
    logger('%O', newTrottinette)

    const trottinette = await maintenance.trottinette.getTrottinetteById(newTrottinette.id)
    logger('%O', "trottinette: ")
    logger('%O', trottinette)
    trottinette.disponible = false
    trottinette.batterie = updatedBatterie

    const updatedTrottinette = await maintenance.trottinette.updateTrottinette(
        trottinette.id,
        trottinette
    )
    logger('%O', "updatedTrottinette: ")
    logger('%O', updatedTrottinette)

    const trottinetteWithIntervention = await maintenance.trottinette.ajouterIntervention(
        trottinette.id,
        { date: Date.now(), description: "Installation batterie" }
    )
    logger('%O', "trottinetteWithIntervention: ")
    logger('%O', trottinetteWithIntervention)

    maintenance.trottinette.deleteTrottinette(trottinette.id)
    // maintenance.batterie.deleteBatterie(batterie.id)
}

async function banque_test() {
    const logger = debug("client:test:banque")

    let comptes = await banque.getAllComptes()

    if (comptes.length > 0) {
        logger('%O', "Il existe déjà des comptes: ")
        logger('%O', comptes)
        return
    }

    const compte1 = await banque.createCompte({ valeur: 1204.0 })
    const compte2 = await banque.createCompte({ valeur: 695.0 })
    const compte3 = await banque.createCompte({ valeur: -43.0 })
    const compte4 = await banque.createCompte({ valeur: 50.0 })

    const autorisation1 = await banque.newAutorisation(
        compte1.id,
        {
            compte: compte2,
            montant: 15.0,
        }
    )
    const autorisation2 = await banque.newAutorisation(
        compte2.id,
        {
            compte: compte1,
            montant: 34.0,
            used: true,
        }
    )
    const autorisation3 = await banque.newAutorisation(
        compte2.id,
        {
            compte: compte3,
            montant: 5.0,
        }
    )

    comptes = await banque.getAllComptes()
    logger('%O', comptes)
}

async function location_test() {
    const logger = debug("client:test:location")

    const emprunts = await location.emprunts.getEmprunts()
    logger('%O', "emprunts: ")
    logger('%O', emprunts)

    const abonnes = await location.abonnes.getAllAbonnes()
    logger('%O', "abonnes: ")
    logger('%O', abonnes)
}

const run = async() => {
    await maintenance_test()
    await banque_test()
    await location_test()
}
run()
