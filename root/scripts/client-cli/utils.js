const fs = require("fs")
const debug = require("debug")

function logRequest(serviceName) {
    const logger = debug(`client:${serviceName}`)

    return function(axiosRequestConfig) {
        const { method, url, baseURL } = axiosRequestConfig
        logger(`${ method.toUpperCase() } ${ baseURL }${ url }`)
        return axiosRequestConfig
    }
}

async function saveJsonToFile(filepath, json) {
    const logger = debug("client:utils:saveJsonToFile")

    return new Promise((resolve, reject) => {
        fs.writeFile(filepath, JSON.stringify(json, null, 4), (err) => {
            if (err) {
                logger(`Une erreur est survenue à l'écriture du fichier ${filepath}`)
                reject(err)
            }
            else {
                logger(`Le fichier ${filepath} a été généré avec succès.`)
                resolve(filepath)
            }
        })
    })
}

async function wait(ms = 5000) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

module.exports = {
    logRequest,
    saveJsonToFile,
    wait,
}
