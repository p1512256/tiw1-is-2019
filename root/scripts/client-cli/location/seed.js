require("dotenv").config()

const path = require("path")
const debug = require("debug")
const logger = debug("client:seed:location")
const {
    getAllAbonnes,
    createAbonne,
    abonnement,
    desabonnement,
} = require("./endpoints/abonnes")
const { saveJsonToFile } = require("../utils")

async function seed() {
    logger("Peuplement de l'API Location...")

    let abonnes = await getAllAbonnes()

    if (abonnes.length > 0) {
        logger("Il existe déjà des abonnés")
    } else {
        let abonne1 = await createAbonne({ name: "Randy" })

        let abonne2 = await createAbonne({ name: "Julien" })
        abonne2 = await abonnement(abonne2.id)

        let abonne3 = await createAbonne({ name: "Juliette" })
        abonne3 = await abonnement(abonne3.id)
        abonne3 = await desabonnement(abonne3.id)

        abonnes = await getAllAbonnes()
    }
    logger('%O', abonnes)

    const samplePath = path.join(__dirname, "location-sample.json")
    saveJsonToFile(samplePath, { abonnes })
}

seed()
