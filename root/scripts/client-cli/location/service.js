const axios = require("axios").default
const { logRequest } = require("../utils")

/**
 * * La variable d'environnement LOCATION_API_URL peut être spécifiée
 * * dans le fichier .env ou au lancement du script npm start
 * * pour indiquer l'endpoint du service d'emprunts.
 *
 * * Valeur par défaut: http://localhost:9090
 */
const locationApiURL = process.env.LOCATION_API_URL || "http://localhost:8082"

const locationService = axios.create({
	baseURL: locationApiURL,
	timeout: 0,
})

locationService.interceptors.request.use(logRequest("service:location"))

module.exports = locationService
