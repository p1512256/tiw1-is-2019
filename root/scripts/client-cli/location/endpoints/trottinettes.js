const locationService = require("../service")

async function getAllTrottinettes() {
    try {
        const response = await locationService.get("/trottinettes")
        return response.data
    } catch (error) { console.error(error) }
}

async function getTrottinette(id) {
    try {
        const response = await locationService.get(`/trottinettes/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function isTrottinetteDisponible(id) {
    try {
        const response = await locationService.get(`/trottinettes/${id}/isDisponible`)
        return response.data
    } catch (error) { console.error(error) }
}

async function takeTrottinette(id) {
    try {
        const response = await locationService.post(`/trottinettes/${id}/takeTrottinette`)
        return response.data
    } catch (error) { console.error(error) }
}

async function freeTrottinette(id) {
    try {
        const response = await locationService.post(`/trottinettes/${id}/freeTrottinette`)
        return response.data
    } catch (error) { console.error(error) }
}

module.exports = {
    getAllTrottinettes,
    getTrottinette,
    isTrottinetteDisponible,
    takeTrottinette,
    freeTrottinette,
}
