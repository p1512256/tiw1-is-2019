const locationService = require("../service")

async function createAbonne(abonne) {
    try {
        const response = await locationService.post("/abonnes", abonne)
        return response.data
    } catch (error) { console.error(error) }
}

async function getAllAbonnes() {
    try {
        const response = await locationService.get("/abonnes")
        return response.data
    } catch (error) { console.error(error) }
}

async function getAbonne(id) {
    try {
        const response = await locationService.get(`/abonnes/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function updateAbonne(id, abonne) {
    try {
        const response = await locationService.put(`/abonnes/${id}`, abonne)
        return response.data
    } catch (error) { console.error(error) }
}

async function abonnement(id) {
    try {
        const response = await locationService.put(`/abonnes/${id}/abonnement`)
        return response.data
    } catch (error) { console.error(error) }
}

async function desabonnement(id) {
    try {
        const response = await locationService.put(`/abonnes/${id}/desabonnement`)
        return response.data
    } catch (error) { console.error(error) }
}

module.exports = {
    createAbonne,
    getAllAbonnes,
    getAbonne,
    updateAbonne,
    abonnement,
    desabonnement,
}
