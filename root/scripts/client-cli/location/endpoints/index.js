const trottinettesEndpoints = require("./trottinettes")
const abonnesEndpoints = require("./abonnes")
const empruntsEndpoints = require("./emprunts")

module.exports = {
    trottinettes: trottinettesEndpoints,
    abonnes: abonnesEndpoints,
    emprunts: empruntsEndpoints,
}
