const locationService = require("../service")

async function getEmprunts({ startDate, endDate } = {}) {
    try {
        const startDateParam = (startDate !== undefined) ? `startDate=${startDate}` : ""
        const endDateParam = (endDate !== undefined) ? `endDate=${endDate}` : ""
        const params = [ startDateParam, endDateParam ].filter(p => p.length > 0).join("&")
        const paramsString = (params.length > 0) ? `?${params}` : ""

        const response = await locationService.get(
            `/emprunts${paramsString}`
        )
        return response.data
    } catch (error) { console.error(error) }
}

async function getEmpruntById(id) {
    try {
        const response = await locationService.get(`/emprunts/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function createEmprunt(emprunt) {
    try {
        const response = await locationService.post("/emprunts", emprunt)
        return response.data
    } catch (error) { console.error(error) }
}

async function requestTransfert(id, transfert) {
    try {
        const response = await locationService.post(`/emprunts/${id}/transfert`, transfert)
        return response.data
    } catch (error) { console.error(error) }
}

module.exports = {
    getEmprunts,
    getEmpruntById,
    createEmprunt,
    requestTransfert,
}
