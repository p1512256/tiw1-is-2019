require("dotenv").config()

const path = require("path")
const debug = require("debug")
const logger = debug("client:seed:maintenance-web")
const {
    getAllBatteries,
    addBatterie,
    updateBatterie,
} = require("./endpoints/batterie")
const {
    getAllTrottinettes,
    addTrottinette,
    updateTrottinette,
    ajouterIntervention,
} = require("./endpoints/trottinette")
const { saveJsonToFile } = require("../utils")

async function seed() {
    logger("Peuplement de l'API Maintenance...")

    let batteries = await getAllBatteries()
    let trottinettes = await getAllTrottinettes()

    if (batteries.length > 0) {
        logger("Il existe déjà des batteries")
    } else {
        let batterie1 = await addBatterie()
        batterie1 = await updateBatterie(batterie1.id, {
            charging: true,
            chargeLevel: 15.0,
        })

        let batterie2 = await addBatterie()
        batterie2 = await updateBatterie(batterie2.id, {
            charging: false,
            chargeLevel: 100.0,
        })

        batteries = await getAllBatteries()
    }
    logger('%O', batteries)

    if (trottinettes.length > 0) {
        logger("Il existe déjà des trottinettes")
    } else {
        let trottinette1 = await addTrottinette()
        trottinette1 = await updateTrottinette(trottinette1.id, { disponible: false })

        let trottinette2 = await addTrottinette()
        trottinette2 = await updateTrottinette(trottinette2.id, {
            disponible: true,
            batterie: batteries[0],
        })

        let trottinette3 = await addTrottinette()
        trottinette3 = await updateTrottinette(trottinette3.id, {
            disponible: true,
            batterie: batteries[1],
        })
        trottinette3 = await ajouterIntervention(trottinette3.id, {
            date: Date.now(),
            description: "Installation batterie",
        })

        trottinettes = await getAllTrottinettes()
    }
    logger('%O', trottinettes)

    const samplePath = path.join(__dirname, "maintenance-sample.json")
    saveJsonToFile(samplePath, { trottinettes, batteries })
}

seed()
