const maintenanceService = require("../service")

async function addBatterie() {
    try {
        const response = await maintenanceService.post("/batterie")
        return response.data
    } catch (error) { console.error(error) }
}

async function getBatterieById(id) {
    try {
        const response = await maintenanceService.get(`/batterie/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function getAllBatteries() {
    try {
        const response = await maintenanceService.get("/batterie")
        return response.data
    } catch (error) { console.error(error) }
}

async function updateBatterie(id, batterie) {
    try {
        const response = await maintenanceService.put(`/batterie/${id}`, batterie)
        return response.data
    } catch (error) { console.error(error) }
}

async function deleteBatterie(id) {
    try {
        const response = await maintenanceService.delete(`/batterie/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

module.exports = {
    addBatterie,
    getBatterieById,
    getAllBatteries,
    updateBatterie,
    deleteBatterie,
}
