const trottinetteEndpoints = require("./trottinette")
const batterieEndpoints = require("./batterie")

module.exports = {
    trottinette: trottinetteEndpoints,
    batterie: batterieEndpoints,
}
