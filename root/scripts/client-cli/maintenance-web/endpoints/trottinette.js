const maintenanceService = require("../service")

async function getAllTrottinettes() {
    try {
        const response = await maintenanceService.get("/trottinette")
        return response.data
    } catch (error) { console.error(error) }
}

async function getTrottinetteById(id) {
    try {
        const response = await maintenanceService.get(`/trottinette/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function addTrottinette() {
    try {
        const response = await maintenanceService.post("/trottinette")
        return response.data
    } catch (error) { console.error(error) }
}

async function updateTrottinette(id, trottinette) {
    try {
        const response = await maintenanceService.put(`/trottinette/${id}`, trottinette)
        return response.data
    } catch (error) { console.error(error) }
}

async function deleteTrottinette(id) {
    try {
        const response = await maintenanceService.delete(`/trottinette/${id}`)
        return response.data
    } catch (error) { console.error(error) }
}

async function ajouterIntervention(id, intervention) {
    try {
        const response = await maintenanceService.post(`/trottinette/${id}/intervention`, intervention)
        return response.data
    } catch (error) { console.error(error) }
}

module.exports = {
    getAllTrottinettes,
    getTrottinetteById,
    addTrottinette,
    updateTrottinette,
    deleteTrottinette,
    ajouterIntervention,
}
