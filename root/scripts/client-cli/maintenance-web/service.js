const axios = require("axios").default
const { logRequest } = require("../utils")

/**
 * * La variable d'environnement MAINTENANCE_API_URL peut être spécifiée
 * * dans le fichier .env ou au lancement du script npm start
 * * pour indiquer l'endpoint du service maintenance.
 *
 * * Valeur par défaut: http://localhost:9091
 */
const maintenanceApiURL = process.env.MAINTENANCE_API_URL || "http://localhost:8080"

const maintenanceService = axios.create({
	baseURL: maintenanceApiURL,
	timeout: 0,
})

maintenanceService.interceptors.request.use(logRequest("service:maintenance-web"))

module.exports = maintenanceService
