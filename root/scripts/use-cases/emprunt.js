require("dotenv").config()

const logger = require("debug")("client:use-case:emprunt")
const { wait } = require("../client-cli/utils")
const { abonnes, emprunts, trottinettes } = require("../client-cli/location/endpoints")
const banque = require("../client-cli/banque/endpoints")

const banqueSample = require("../client-cli/banque/banque-sample.json")
const locationSample = require("../client-cli/location/location-sample.json")
const maintenanceSample = require("../client-cli/maintenance-web/maintenance-sample.json")

const [ compteEntrepriseLocation, compteUser ] = banqueSample.comptes
const [ user ] = locationSample.abonnes
const [ , , trottinette ] = maintenanceSample.trottinettes

async function run() {
    logger("compteEntrepriseLocation ---------------------------------------------------")
    logger('%O', compteEntrepriseLocation)
    logger("compteUser ---------------------------------------------------")
    logger('%O', compteUser)
    logger("user ---------------------------------------------------")
    logger('%O', user)
    logger("trottinette ---------------------------------------------------")
    logger('%O', trottinette)

    // Create emprunt
    const emprunt = await emprunts.createEmprunt({
        idAbonne: user.id,
        idTrottinette: trottinette.id,
    })
    logger("emprunt ---------------------------------------------------")
    logger('%O', emprunt)

    // Create autorisation
    const autorisation = await banque.newAutorisation(compteUser.id, { montant: emprunt.montant })
    logger("autorisation ---------------------------------------------------")
    logger('%O', autorisation)

    // Request transfert
    const isTransfertOk = await emprunts.requestTransfert(emprunt.id, {
        from: compteUser.id,
        to: compteEntrepriseLocation.id,
        autorisation: autorisation.id
    })
    logger("isTransfertOk ---------------------------------------------------")
    logger('%O', isTransfertOk)

    // Check if transfert ok
    if (!isTransfertOk) {
        logger("Paiement refusé.")
        return
    }

    logger("Paiement accepté !")
    logger("En attente d'activation de l'emprunt...")

    let attempts = 0
    let empruntActif = false
    do {
        const pendingEmprunt = await emprunts.getEmpruntById(emprunt.id)
        empruntActif = pendingEmprunt.actif
        attempts++
        if (empruntActif) {
            logger("Emprunt activé.")
        } else {
            logger(`Emprunt inactif, en attente d'activation de l'emprunt... (${attempts} tentative${attempts > 1 ? "s" : ""})`)
            await wait(1000)
        }
    } while (attempts < 10 && !empruntActif)
}

run()
